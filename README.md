# model2

Service used to manage model and related revision.

## Basic concepts

## Domain model

## How to develop

## Todo

### Refactor `Revision` model
The concept **`RevisionObject`** is not very intuitive. Actually, what we want to express here is a working model.

Initially, this working model contains all objects copied from revised model. Over time, this model will contain newly created objects
and a revised objects along the revision process.

With this working model, query for all objects in revision, merge and rebase operation will become more straightforward.