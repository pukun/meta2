package io.odsp;

import io.odsp.api.rest.Draft;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.reactive.server.WebTestClient;

@SpringBootTest
@AutoConfigureWebTestClient
@ActiveProfiles("docker")
@DisplayName("verify configurations across the app is correctly assembled")
public class Model2AppTest {

    @Test
    public void startApp(@Autowired WebTestClient webClient) {
        webClient
                .get().uri("/v1/revisions/1/drafts")
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(Draft.class).hasSize(0);
    }
}
