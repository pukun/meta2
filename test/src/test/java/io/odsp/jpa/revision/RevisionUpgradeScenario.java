package io.odsp.jpa.revision;

import io.odsp.domain.common.Scope;
import io.odsp.domain.model.Model;
import io.odsp.domain.revision.Revision;

import static io.odsp.jpa.model.ModelEntry.use;
import static io.odsp.jpa.revision.RevisionOperation.*;
import static io.odsp.jpa.revision.RevisionSnapshot.introduce;
import static io.odsp.jpa.revision.RevisionSnapshot.promote;

public record RevisionUpgradeScenario(RevisionOps ops) {
    /**
     * <pre>
     *     =================================================
     *             Prepare universal model
     *     =================================================
     *     1. given a universal model u1: [a, b, c, d, e];
     *     2. revise u1 by removing 'b' and 'e', producing u2 which contains: [a, b(removed), c, d, e(removed)];
     *     3. revise u2 by removing 'c' and creating 'f', producing u3 which contains: [a, c(removed), d, f];
     *
     *     =================================================
     *              Prepare proprietary model
     *     =================================================
     *     4. create proprietary model l1 from u1, contains: [a, b, c, d, e]
     *     5. revise l1 by removing 'd' and removing 'e', producing l2 which contains: [a, b, c, d(removed), e(removed)]
     *     6. revise l2 by modifying 'b' and creating 'g', producing l3 which contains: [a, b(1), c, g]
     *
     *     =================================================
     *              Carry out upgrade
     *     =================================================
     *     7. upgrade l3 with u3, the related revision contains snapshots: [upgrade(a), upgrade(b, 1, 1), upgrade(c, 1), upgrade(d, 1, 0), introduce(f)]
     *     8. carry out the upgrade, produced model l4 contains: [a(1), b(2), c(1), d(2), f(0), g(0)]
     * </pre>
     */
    public void upgrade() {
        // initialize model u1
        Model u1 = ops.modelOf(use("a"), use("b"), use("c"), use("d"), use("e"));

        // revise u1
        Model u2 = ops.reviseAndThenApply(u1, remove("b"), remove("e"));

        // revise u2
        Model u3 = ops.reviseAndThenApply(u2, remove("c"), create("f"));

        // create l1
        Model l1 = ops.createEpochModel(Scope.SH, u1);

        // revise l2
        Model l2 = ops.reviseAndThenApply(l1, remove("d"), remove("e"));

        // revise l2
        Model l3 = ops.reviseAndThenApply(l2, modify("b"), create("g"));

        // upgrade
        Revision upgrade = ops.revisionForUpgrade(Scope.SH, u3);
        ops.assertSnapshotOfRevisionContains(
                upgrade,
                promote("a"), promote("b", 1, 1), promote("c", 1),
                promote("d", 1, 0), introduce("f")
        );
        ops.assertRevisionContainsObjects(
                upgrade,
                use("a", 1), use("b", 2), use("c", 1),
                use("d", 2), use("f"),
                use("g")
        );
    }
}
