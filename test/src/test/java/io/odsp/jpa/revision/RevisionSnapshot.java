package io.odsp.jpa.revision;

import io.odsp.domain.meta.MetaObject;
import io.odsp.domain.meta.MetaObjectCode;
import io.odsp.domain.meta.MetaObjectEdition;
import io.odsp.domain.revision.Change;

import java.util.Optional;

record RevisionSnapshot(MetaObjectCode code, MetaObjectEdition prototype, MetaObjectEdition reference,
                        boolean conflicting) {

    static RevisionSnapshot from(Change change) {
        return Optional.ofNullable(change.prototype())
                .map(prototype -> new RevisionSnapshot(
                        prototype.code(), prototype.edition(),
                        Optional.ofNullable(change.reference()).map(MetaObject::edition).orElse(null),
                        change.isConflicting()))
                .orElse(Optional.ofNullable(change.reference())
                        .map(reference -> introduce(change.product().code().code(), reference.edition().edition()))
                        .orElse(creation(change.product().code().code()))
                );
    }

    static RevisionSnapshot creation(String code) {
        return new RevisionSnapshot(MetaObjectCode.of(code), null, null, false);
    }

    static RevisionSnapshot conflict(String code) {
        return new RevisionSnapshot(MetaObjectCode.of(code), MetaObjectEdition.InitialEdition, null, true);
    }

    static RevisionSnapshot conflict(String code, int prototype) {
        return new RevisionSnapshot(MetaObjectCode.of(code), MetaObjectEdition.of(prototype), null, true);
    }

    static RevisionSnapshot modification(String code) {
        return new RevisionSnapshot(MetaObjectCode.of(code), MetaObjectEdition.InitialEdition, null, false);
    }

    static RevisionSnapshot modification(String code, int prototype) {
        return new RevisionSnapshot(MetaObjectCode.of(code), MetaObjectEdition.of(prototype), null, false);
    }

    static RevisionSnapshot introduce(String code) {
        return introduce(code, MetaObjectEdition.InitialEdition.edition());
    }

    static RevisionSnapshot introduce(String code, int reference) {
        return new RevisionSnapshot(MetaObjectCode.of(code), null, MetaObjectEdition.of(reference), false);
    }

    static RevisionSnapshot promote(String code) {
        return promote(code, MetaObjectEdition.InitialEdition.edition());
    }

    static RevisionSnapshot promote(String code, int reference) {
        return promote(code, MetaObjectEdition.InitialEdition.edition(), reference);
    }

    static RevisionSnapshot promote(String code, int prototype, int reference) {
        return new RevisionSnapshot(MetaObjectCode.of(code), MetaObjectEdition.of(prototype), MetaObjectEdition.of(reference), false);
    }
}
