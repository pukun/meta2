package io.odsp.jpa.revision;

import io.odsp.domain.model.Model;
import io.odsp.domain.revision.Revision;

import static io.odsp.jpa.model.ModelEntry.drop;
import static io.odsp.jpa.model.ModelEntry.use;
import static io.odsp.jpa.revision.RevisionOperation.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;


public record RevisionApplyScenario(RevisionOps ops) {
    /**
     * =================================================
     * <pre>
     *
     * 1. create revision(ru0): creation['a', 'b'(<b>removed</b>)]
     * 2. apply ru0 and produce new model 'u1';
     * 3. verify model 'u1' contains: 'a', 'b'(<b>removed</b>)
     *
     * </pre>
     * =================================================
     */
    public void applyRevisionOfEpochModel() {

        // create revision of epoch model
        Revision ru0 = ops.revisionOf(create("a"), remove("b"));

        // revision contains 'a' and 'b'
        ops.assertRevisionContainsObjects(ru0, use("a"), drop("b"));

        // apply it
        ops.apply(ru0);

        // product model only contains 'a' since 'b' is removed in revision
        ops.assertModelContainsObjects(ru0.productModel(), use("a"));
    }


    /**
     * =================================================
     * <pre>
     *
     * 1. create an universal model(u1) : a, b, c, d(<b>removed</b>);
     * 2. create revision(ru1) against u1: creation[e] + modification[b] +  removal[c]
     * 3. apply ru1 and produce new model(u2)
     * 4. verify model u2 contains: a, b2, c2(removed), e
     *
     * </pre>
     * =================================================
     */
    public void applyRevisionOfExistingModel() {

        // initialize a model: [a, b, c, d(removed)]
        Model u1 = ops.modelOf(use("a"), use("b"), use("c"), drop("d"));

        // given a revision with: creation[e] + modification[b] +  removal[c]
        Revision r1 = ops.revisionOf(u1, create("e"), modify("b"), remove("c"));

        // verify entries of revision should contain: [a, b(edition=1), c(edition=1, removed=true), e]
        ops.assertRevisionContainsObjects(r1, use("a"), use("b", 1), drop("c", 1), use("e"));

        // apply
        ops.apply(r1);

        // now there is a new model(edition =2)
        Model u2 = ops.context().latestUniversalModel().orElse(u1);
        assertThat(u2.edition(), is(u1.edition().next()));

        // and it contains entities: [a, b(edition=1), c(edition=1, removed=true), e]
        ops.assertModelContainsObjects(u2, use("a"), use("b", 1), drop("c", 1), use("e"));
    }

}
