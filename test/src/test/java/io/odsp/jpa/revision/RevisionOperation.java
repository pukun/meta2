package io.odsp.jpa.revision;

record RevisionOperation(String code, int operation) {

    static final int CREATION = 0;
    static final int MODIFICATION = 1;
    static final int REMOVAL = 2;

    boolean isCreation() {
        return operation == CREATION;
    }

    boolean isModification() {
        return operation == MODIFICATION;
    }

    boolean isRemoval() {
        return operation == REMOVAL;
    }

    static RevisionOperation create(String code) {
        return new RevisionOperation(code, CREATION);
    }

    static RevisionOperation modify(String code) {
        return new RevisionOperation(code, MODIFICATION);
    }

    static RevisionOperation remove(String code) {
        return new RevisionOperation(code, REMOVAL);
    }
}
