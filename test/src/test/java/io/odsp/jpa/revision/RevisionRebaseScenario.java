package io.odsp.jpa.revision;

import io.odsp.domain.model.Model;
import io.odsp.domain.revision.Revision;

import static io.odsp.jpa.model.ModelEntry.drop;
import static io.odsp.jpa.model.ModelEntry.use;
import static io.odsp.jpa.revision.RevisionOperation.*;
import static io.odsp.jpa.revision.RevisionSnapshot.*;

public record RevisionRebaseScenario(RevisionOps ops) {

    /**
     * <pre>
     *    1. given an initial model u1: [a, b, c, d]
     *    2. revision r1: modify a, b; remove c and create e
     *    3. u1 changed to u2 with modification(b) and removal(c)
     *    4. u2 changed to u3 with revision r3, b is modified again and f is added
     *    5. after being rebased, b and c is marked as conflict, a is left unchanged
     * </pre>
     */
    public void rebase() {
        // Initialize model with: [a, b, c, d]
        Model u1 = ops.modelOf(use("a"), use("b"), use("c"), use("d"));

        // A revision: modify[a, b],remove[c], create[e]
        Revision r1 = ops.revisionOf(u1, modify("a"), modify("b"), remove("c"), create("e"));

        // update u1 to u2 by modifying b, removing c and adding f
        Model u2 = ops.reviseAndThenApply(u1, modify("b"), remove("c"), create("f"));

        // update u2 to u3 by modifying b again
        Model u3 = ops.reviseAndThenApply(u2, modify("b"));

        // rebase r1 to u3
        ops.rebase(r1);

        // verify revision contains changes: [("a", 0), ("b", 2, conflict), ("c", 2, conflict), ("d", -1)]
        ops.assertSnapshotOfRevisionContains(r1, modification("a"), conflict("b", 2), conflict("c", 1), creation("e"));

        // verify revision rebased to u3, so it contains objects: ['a'(1), 'b'(1), 'c'(1, removed), 'd'(0), 'e'(0), 'f'(0)]
        ops.assertRevisionContainsObjects(r1,
                use("a", 1), use("b", 1), drop("c", 1), use("e"),
                use("d"), use("f"));
    }


}
