package io.odsp.jpa.revision;

import io.odsp.domain.meta.MetaObject;
import io.odsp.domain.model.Model;
import io.odsp.domain.model.ModelIdentity;
import io.odsp.domain.revision.*;
import io.odsp.jpa.meta.MetaObjectProvider;

import java.util.*;

import static io.odsp.jpa.meta.MetaObjectProvider.entityOf;


public record RevisionBuilder(RevisionContext context) {


    RevisionCreation.Ok revisionOf(RevisionOperation... operations) {
        return revisionOf(ModelIdentity.epoch(), null, operations);
    }

    RevisionCreation.Ok revisionOf(ModelIdentity prototype, RevisionOperation... operations) {
        return revisionOf(prototype, null, operations);
    }

    RevisionCreation.Ok revisionOf(ModelIdentity identity, Model reference, RevisionOperation... operations) {

        Revision revision = new Revision().scope(identity.scope()).edition(identity.edition())
                .referenceModel(reference)
                .status(RevisionStatus.REVISING);

        Map<String, MetaObject> prototypes = new HashMap<>();
        context.modelContext().findModelByIdentity(revision.identityOfPrototypeModel())
                .map(model -> context.modelContext().findActiveObjectsInModel(model))
                .ifPresent(objects -> objects.forEach(object -> prototypes.put(object.code().code(), object)));

        List<Change> changes = new ArrayList<>();

        List<RevisionOperation> operationList = Arrays.asList(operations);

        operationList.stream()
                .filter(RevisionOperation::isCreation)
                .map(RevisionOperation::code)
                .map(MetaObjectProvider::entityOf)
                .map(revision::changeOfCreation)
                .forEach(changes::add);

        operationList.stream()
                .filter(RevisionOperation::isModification)
                .map(RevisionOperation::code)
                .map(prototypes::get)
                .map(revision::changeOfModification)
                .forEach(changes::add);

        operationList.stream()
                .filter(RevisionOperation::isRemoval)
                .map(RevisionOperation::code)
                .map(removal -> prototypes.containsKey(removal) ?
                        revision.changeOfModification(prototypes.get(removal)) :
                        revision.changeOfCreation(entityOf(removal))
                ).map(Change::remove)
                .forEach(changes::add);


        return RevisionCreation.ok(revision, changes);
    }
}
