package io.odsp.jpa.revision;

import io.odsp.domain.common.Scope;
import io.odsp.domain.meta.MetaObject;
import io.odsp.domain.model.Model;
import io.odsp.domain.model.ModelIdentity;
import io.odsp.domain.revision.*;
import io.odsp.jpa.model.ModelBuilder;
import io.odsp.jpa.model.ModelEntry;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;

public record RevisionOps(EntityManager entityManager, RevisionRepository repository, RevisionContext context) {


    Model modelOf(ModelEntry... entries) {
        return new ModelBuilder(entityManager, context.objectContext(), context.modelContext())
                .modelOf(entries);
    }

    Revision revisionOf(RevisionOperation... operations) {
        return save(buildRevisionAndChanges(operations));
    }

    Revision revisionOf(Model u1, RevisionOperation... operations) {
        return save(buildRevisionAndChanges(u1.identityOf(), operations));
    }


    Revision revisionForUpgrade(Scope scope, Model u) {
        RevisionCreation result = factory().upgrade(scope, u.edition());
        assertThat(result.isOk(), is(true));

        return save((RevisionCreation.Ok) result);
    }

    void rebase(Revision r) {
        RebaseResult result = r.rebase(context);
        assertThat(result.isOk(), is(true));

        repository.save(r);
        entityManager.flush();
        entityManager.clear();
    }

    void apply(Revision revision) {
        Optional<ApplyingFailure> failure = revision.apply(context);
        assertThat(failure.isEmpty(), is(true));

        repository.save(revision);
        entityManager.flush();
        entityManager.clear();
    }

    Model reviseAndThenApply(Model prototype, RevisionOperation... operations) {
        Revision r = revisionOf(prototype, operations);

        apply(r);

        return r.productModel();
    }

    Model createEpochModel(Scope scope, Model u) {
        Revision revision = save(factory().reviseEpochModel(scope, u));

        apply(revision);

        return revision.productModel();
    }


    private RevisionCreation.Ok buildRevisionAndChanges(RevisionOperation... operations) {
        return builder().revisionOf(operations);
    }

    private RevisionCreation.Ok buildRevisionAndChanges(Model u1, RevisionOperation... operations) {
        return builder().revisionOf(u1.identityOf(), operations);
    }

    private RevisionCreation.Ok buildRevisionAndChanges(ModelIdentity u1, RevisionOperation... operations) {
        return builder().revisionOf(u1, operations);
    }


    private RevisionBuilder builder() {
        return new RevisionBuilder(context);
    }

    private RevisionFactory factory() {
        return new RevisionFactory(repository, context);
    }

    private Revision save(RevisionCreation.Ok creation) {
        Revision revision = creation.revision();
        List<Change> changes = creation.changes();

        repository.save(revision);

        int batch = 1_000;
        for (int i = 0; i < changes.size(); ) {
            int end = Integer.min(changes.size(), i + batch);

            context.changeRepository().saveAll(changes.subList(i, end));
            entityManager.flush();
            entityManager.clear();

            i = end;
        }

        return revision;
    }

    void assertRevisionContainsObjects(Revision r1, ModelEntry... entries) {
        verifyObjects(context.entryRepository().findAllObjectsInRevision(r1), entries);
    }

    void assertModelContainsObjects(Model m, ModelEntry... entries) {
        verifyObjects(context.modelContext().findAllObjectsInModel(m), entries);
    }

    void assertSnapshotOfRevisionContains(Revision r, RevisionSnapshot... snapshots) {
        List<Change> changes = context.changeRepository().findAllChangesInRevision(r);

        assertThat(changes.size(), is(snapshots.length));
        assertThat(changes.stream().map(RevisionSnapshot::from).toList(), containsInAnyOrder(snapshots));
    }

    private void verifyObjects(List<MetaObject> objects, ModelEntry... entries) {
        assertThat(objects.size(), is(entries.length));
        assertThat(objects.stream().map(ModelEntry::from).toList(), containsInAnyOrder(entries));
    }
}
