package io.odsp.jpa.model;

import io.odsp.domain.meta.MetaObject;
import io.odsp.domain.meta.MetaObjectContext;
import io.odsp.domain.model.Model;
import io.odsp.domain.model.ModelContext;
import org.apache.commons.lang3.tuple.Pair;

import javax.persistence.EntityManager;
import java.util.List;

import static io.odsp.jpa.model.ModelEntry.drop;
import static io.odsp.jpa.model.ModelEntry.use;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;

public record ModelScenario(EntityManager entityManager, ModelContext modelContext, MetaObjectContext objectContext) {

    /**
     * =================================================
     * <p>
     * 1. create a universal model with two objects: 'a', 'b'(removed = true)
     * 2. verify all objects can be accessed from the model;
     * 3. verify all active objects can be accessed;
     * 4. verify all universal objects can be accessed
     * <p>
     * =================================================
     */
    public void accessModelAndEntries() {
        // create a model
        Model model = modelOf(use("a", 2), drop("b", 3));

        assertThat(modelContext.latestUniversalModel().isPresent(), is(true));
        assertThat(modelContext.repository().findModelByIdentity(model.identityOf()).isPresent(), is(true));

        // check all entities in the model
        allObjectsCanBeAccessed(model, Pair.of("a", 2), Pair.of("b", 3));
        activeObjectsCanBeAccessed(model, Pair.of("a", 2));
        universalObjectsCanBeAccessed(model, Pair.of("a", 2));

    }

    private Model modelOf(ModelEntry... entries) {
        return new ModelBuilder(entityManager, objectContext, modelContext)
                .modelOf(entries);
    }

    private void allObjectsCanBeAccessed(Model model, Pair<String, Integer>... objects) {
        List<MetaObject> entities = modelContext.findAllObjectsInModel(model);
        assertThat(entities.size(), is(2));
        assertThat(entities.stream().map(object -> Pair.of(object.label(), object.edition().edition())).toList(), contains(objects));
    }

    private void activeObjectsCanBeAccessed(Model model, Pair<String, Integer>... objects) {
        List<MetaObject> entities = modelContext.findActiveObjectsInModel(model);
        assertThat(entities.size(), is(1));
        assertThat(entities.stream().map(object -> Pair.of(object.label(), object.edition().edition())).toList(), contains(objects));
    }

    private void universalObjectsCanBeAccessed(Model model, Pair<String, Integer>... objects) {
        List<MetaObject> entities = modelContext.findActiveUniversalObjectsInModel(model);
        assertThat(entities.size(), is(1));
        assertThat(entities.stream().map(object -> Pair.of(object.label(), object.edition().edition())).toList(), contains(objects));
    }
}
