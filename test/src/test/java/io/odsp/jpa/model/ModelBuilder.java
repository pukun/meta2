package io.odsp.jpa.model;

import io.odsp.domain.common.Scope;
import io.odsp.domain.meta.MetaObject;
import io.odsp.domain.meta.MetaObjectContext;
import io.odsp.domain.meta.MetaObjectEdition;
import io.odsp.domain.model.Model;
import io.odsp.domain.model.ModelContext;
import io.odsp.domain.model.ModelEdition;
import io.odsp.jpa.meta.MetaObjectProvider;

import javax.persistence.EntityManager;
import java.util.Arrays;
import java.util.List;

public record ModelBuilder(EntityManager entityManager, MetaObjectContext objectContext, ModelContext modelContext) {

    public Model modelOf(ModelEntry... entries) {
        return modelOf(ModelEdition.epoch().next(), entries);
    }

    public Model modelOf(ModelEdition edition, ModelEntry... entries) {
        return build(Scope.UNIVERSALITY, edition, null, entries);
    }


    private Model build(Scope scope, ModelEdition edition, Model reference, ModelEntry... entries) {
        Model model = new Model()
                .scope(scope)
                .edition(edition)
                .reference(reference)
                .isLatest(true);

        List<MetaObject> objects = Arrays.stream(entries).map(entry -> MetaObjectProvider
                .entityOf(entry.code())
                .edition(MetaObjectEdition.of(entry.edition()))
                .isRemoved(entry.removed())
        ).toList();

        modelContext.repository().save(model);
        objectContext.repository().saveAll(objects);
        modelContext.entryRepository().saveAll(
                objects.stream().map(model::entryOf).toList()
        );

        entityManager.flush();
        entityManager.clear();

        return model;
    }
}
