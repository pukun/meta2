package io.odsp.jpa.model;

import io.odsp.domain.meta.MetaObject;
import io.odsp.domain.meta.MetaObjectEdition;

public record ModelEntry(String code, int edition, boolean removed) {

    public static ModelEntry from(MetaObject object) {
        return new ModelEntry(object.code().code(), object.edition().edition(), object.isRemoved());
    }

    public static ModelEntry use(String code, int edition) {
        return new ModelEntry(code, edition, false);
    }

    public static ModelEntry use(String code) {
        return use(code, MetaObjectEdition.InitialEdition.edition());
    }

    public static ModelEntry drop(String code) {
        return drop(code, MetaObjectEdition.InitialEdition.edition());
    }

    public static ModelEntry drop(String code, int edition) {
        return new ModelEntry(code, edition, true);
    }
}
