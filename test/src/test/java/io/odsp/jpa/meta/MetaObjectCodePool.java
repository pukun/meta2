package io.odsp.jpa.meta;

import io.odsp.domain.meta.MetaObjectCode;
import io.odsp.domain.meta.MetaObjectCodeGenerator;
import io.odsp.domain.meta.MetaObjectType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MetaObjectCodePool implements MetaObjectCodeGenerator {

    private final List<String> pool = new ArrayList<>();
    private int next;

    public MetaObjectCodePool(String... codes) {
        pool.addAll(Arrays.asList(codes));
    }

    public static MetaObjectCodePool from(String... codes) {
        return new MetaObjectCodePool(codes);
    }

    @Override
    public MetaObjectCode code(MetaObjectType objectType) {
        if (next < pool.size()) return MetaObjectCode.of(pool.get(next++));
        return null;
    }

}
