package io.odsp.jpa.meta;

import io.odsp.domain.meta.*;
import org.hamcrest.CoreMatchers;

import javax.persistence.EntityManager;
import java.util.List;

import static io.odsp.jpa.meta.MetaObjectProvider.Attributes;
import static io.odsp.jpa.meta.MetaObjectProvider.entityOf;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public record MetaObjectScenario(EntityManager manager, MetaObjectContext context,
                                 MetaObjectCodeGenerator codeGenerator) {

    public void findEntityByCode() {
        final MetaEntity entity = entityOf("entity1");
        context.repository().save(entity);

        manager.flush();
        manager.clear();

        List<MetaObject> loaded = context.repository().findByCode(entity.code());
        assertThat(loaded.size(), CoreMatchers.is(1));
        assertThat(loaded.contains(entity), CoreMatchers.is(true));

        MetaEntity saved = (MetaEntity) loaded.get(0);
        assertThat(
                saved.getAttributes().stream()
                        .map(MetaEntityAttribute::getLabel)
                        .toList(),
                contains(Attributes.stream()
                        .map(MetaEntityAttribute::getLabel)
                        .toArray(String[]::new)
                )
        );
    }

    public void generateCode() {
        MetaObjectCode aCode = codeGenerator.codeOfAssociation();
        assertThat(aCode.code(), startsWith("a"));
        MetaObjectCode eCode = codeGenerator.codeOfEntity();
        assertThat(eCode.code(), startsWith("e"));

        long aSeq = Long.parseLong(aCode.code().substring(2));
        long eSeq = Long.parseLong(eCode.code().substring(2));
        assertThat(aSeq, lessThan(eSeq));
    }
}
