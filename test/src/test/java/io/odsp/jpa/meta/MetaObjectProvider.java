package io.odsp.jpa.meta;

import io.odsp.domain.meta.*;

import java.util.Arrays;
import java.util.List;

public class MetaObjectProvider {

    public final static List<MetaEntityAttribute> Attributes = Arrays.asList(
            MetaEntityAttribute.of(0, "a1", MetaEntityAttributeType.LITERAL),
            MetaEntityAttribute.of(1, "a2", MetaEntityAttributeType.NUMBER)
    );

    public static MetaEntity entityOf(String code) {
        return entityOf(MetaObjectCode.of(code), creationSpecOf(code));
    }

    public static MetaEntity entityOf(MetaObjectCode code, MetaEntityCreationSpec spec) {
        return (MetaEntity) MetaEntity.from(code, spec);
    }

    public static MetaEntityCreationSpec creationSpecOf(String code) {
        return new MetaEntityCreationSpec(true, code, Attributes);
    }

    public static MetaEntityModificationSpec modificationSpecOf(String code) {
        return new MetaEntityModificationSpec(code + "'", Attributes);
    }
}
