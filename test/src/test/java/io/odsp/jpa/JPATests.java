package io.odsp.jpa;

import io.odsp.domain.meta.MetaObjectCodeGenerator;
import io.odsp.domain.meta.MetaObjectContext;
import io.odsp.domain.meta.MetaObjectFactory;
import io.odsp.domain.meta.MetaObjectRepository;
import io.odsp.domain.model.ModelContext;
import io.odsp.domain.model.ModelEntryRepository;
import io.odsp.domain.model.ModelFactory;
import io.odsp.domain.model.ModelRepository;
import io.odsp.domain.revision.*;
import io.odsp.jpa.meta.MetaObjectCodePool;
import io.odsp.jpa.meta.MetaObjectScenario;
import io.odsp.jpa.model.ModelScenario;
import io.odsp.jpa.revision.RevisionApplyScenario;
import io.odsp.jpa.revision.RevisionOps;
import io.odsp.jpa.revision.RevisionRebaseScenario;
import io.odsp.jpa.revision.RevisionUpgradeScenario;
import io.odsp.service.JPAConfig;
import io.odsp.service.meta.MetaObjectConfig;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@ActiveProfiles("jpa")
@DataJpaTest
@ContextConfiguration(classes = {JPAConfig.class, MetaObjectConfig.class})
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@DisplayName("data access tests against postgresql container")
public class JPATests {
    @PersistenceContext
    EntityManager entityManager;

    @Autowired
    private MetaObjectRepository objectRepository;

    @Autowired
    private ModelRepository modelRepository;

    @Autowired
    private ModelEntryRepository modelEntryRepository;

    @Autowired
    private RevisionRepository revisionRepository;

    @Autowired
    private ChangeRepository changeRepository;

    @Autowired
    private MetaObjectCodeGenerator metaObjectCodeGenerator;

    @Autowired
    private RevisionEntryRepository revisionEntryRepository;

    private MetaObjectContext objectContext;

    private ModelContext modelContext;

    private RevisionContext revisionContext;

    private RevisionFactory revisionFactory;

    @PostConstruct
    public void init() {
        MetaObjectCodePool metaObjectCodePool = new MetaObjectCodePool();
        objectContext = new MetaObjectContext(new MetaObjectFactory(metaObjectCodePool), objectRepository);

        modelContext = new ModelContext(new ModelFactory(), modelRepository, modelEntryRepository);

        revisionContext = new RevisionContext(
                objectContext,
                modelContext,
                revisionEntryRepository,
                changeRepository
        );

        revisionFactory = new RevisionFactory(revisionRepository, revisionContext);
    }

    @Nested
    @DisplayName("entity repository")
    public class MetaObjectRepositoryTest {
        private final MetaObjectScenario scenario = new MetaObjectScenario(entityManager, objectContext, metaObjectCodeGenerator);

        @Test
        @DisplayName("find entity by code")
        public void findEntityByCode() {
            scenario.findEntityByCode();
        }

        @Test
        @DisplayName("generate code with sequence")
        public void generateCode() {
            scenario.generateCode();
        }
    }

    @Nested
    @DisplayName("model repository")
    class ModelRepositoryTest {
        private final ModelScenario scenario = new ModelScenario(entityManager, modelContext, objectContext);

        @Test
        @DisplayName("access entities in model")
        public void accessEntitiesInModel() {
            scenario.accessModelAndEntries();
        }
    }

    @Nested
    @DisplayName("revision repository")
    class RevisionRepositoryTest {

        private RevisionOps ops = new RevisionOps(entityManager, revisionRepository, revisionContext);

        @Nested
        @DisplayName("apply revision")
        class RevisionApplyTest {

            private final RevisionApplyScenario scenario = new RevisionApplyScenario(ops);

            @Test
            @DisplayName("apply revision of epoch model")
            public void applyRevisionOfEpochModel() {
                scenario.applyRevisionOfEpochModel();
            }

            @Test
            @DisplayName("apply revision of existing model")
            public void applyRevisionOfExistingModel() {
                scenario.applyRevisionOfExistingModel();
            }
        }

        @Nested
        @DisplayName("rebase revision")
        class RevisionRebaseTest {
            private final RevisionRebaseScenario scenario = new RevisionRebaseScenario(ops);

            @Test
            @DisplayName("rebase revision")
            public void rebase() {
                scenario.rebase();
            }
        }

        @Nested
        @DisplayName("upgrade model")
        class RevisionUpgradeTest {
            private final RevisionUpgradeScenario scenario = new RevisionUpgradeScenario(ops);

            @Test
            @DisplayName("generate revision for upgrade and then apply it")
            public void upgrade() {
                scenario.upgrade();
            }
        }
    }
}
