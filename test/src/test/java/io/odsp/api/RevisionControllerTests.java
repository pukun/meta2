package io.odsp.api;

import io.odsp.api.rest.ModelId;
import io.odsp.api.rest.Revision;
import io.odsp.api.revision.RevisionController;
import io.odsp.domain.revision.ApplyingFailure;
import io.odsp.service.ReactiveConfig;
import io.odsp.service.revision.RevisionService;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@WebFluxTest(RevisionController.class)
@Import(ReactiveConfig.class)
public class RevisionControllerTests {
    @Autowired
    private WebTestClient webTestClient;

    @MockBean
    private RevisionService revisionService;

    @Nested
    @DisplayName("merge tests")
    public class MergeTests {

        @Test
        @DisplayName("when merge succeeds")
        void whenMergeIsOk() {
            Revision revision = mock(Revision.class);
            when(revision.id()).thenReturn(1l);
            when(revision.revisedModel()).thenReturn(new ModelId("JX", 12));
            when(revision.producedModel()).thenReturn(new ModelId("JX", 13));
            when(revision.status()).thenReturn("APPLIED");

            given(revisionService.apply(1))
                    .willReturn(Pair.of(revision, Optional.empty()));

            WebTestClient.BodyContentSpec mergeResponse = webTestClient
                    .post().uri("/v1/revisions/1/merge")
                    .contentType(MediaType.APPLICATION_JSON)
                    .exchange()
                    .expectStatus().isOk()
                    .expectBody();
            mergeResponse
                    .jsonPath("$.id").isEqualTo("1")
                    .jsonPath("$.revisedModel.scope").isEqualTo("JX")
                    .jsonPath("$.revisedModel.edition").isEqualTo(12)
                    .jsonPath("$.producedModel.edition").isEqualTo(13);
        }

        @Test
        @DisplayName("when revision is with conflict")
        void whenRevisionIsWithConflict() {

            given(revisionService.apply(1))
                    .willReturn(Pair.of(mock(Revision.class), Optional.of(ApplyingFailure.withConflict())));

            WebTestClient.BodyContentSpec mergeResponse = webTestClient
                    .post().uri("/v1/revisions/1/merge")
                    .contentType(MediaType.APPLICATION_JSON)
                    .exchange()
                    .expectStatus().isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY)
                    .expectBody();
            mergeResponse
                    .jsonPath("$.code").isEqualTo("withConflict")
                    .jsonPath("$.message").isEqualTo(ApplyingFailure.withConflict().detail());
        }
    }
}
