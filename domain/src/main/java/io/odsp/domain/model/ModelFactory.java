package io.odsp.domain.model;

import io.odsp.domain.common.Scope;

public record ModelFactory() {

    public Model sinceEpoch() {
        Model model = new Model();

        model.scope = Scope.UNIVERSALITY;
        model.edition = ModelEdition.epoch().next();
        model.isLatest = true;

        return model;
    }

    public Model sinceEpoch(Scope scope, Model upstream) {
        if (scope == Scope.UNIVERSALITY) throw new IllegalArgumentException("Not qualified proprietary scope");

        if (upstream == null || upstream.scope() != Scope.UNIVERSALITY)
            throw new IllegalArgumentException("Not given a valid upstream");

        Model model = new Model();

        model.scope = scope;
        model.edition = ModelEdition.epoch().next();
        model.reference = upstream;
        model.isLatest = true;

        return model;
    }

    public Model successorOf(Model revisedModel) {
        Model model = new Model();

        model.edition = revisedModel.edition.next();
        model.scope = revisedModel.scope;
        model.reference = revisedModel.reference;
        model.isLatest = true;

        return model;
    }

}
