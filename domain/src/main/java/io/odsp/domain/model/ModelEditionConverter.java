package io.odsp.domain.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class ModelEditionConverter implements AttributeConverter<ModelEdition, Integer> {
    @Override
    public Integer convertToDatabaseColumn(ModelEdition modelEdition) {
        return modelEdition == null ? null : modelEdition.edition();
    }

    @Override
    public ModelEdition convertToEntityAttribute(Integer modelEdition) {
        return modelEdition == null ? null : new ModelEdition(modelEdition);
    }
}
