package io.odsp.domain.model;

import io.odsp.domain.common.Scope;

public record ModelIdentity(Scope scope, ModelEdition edition) {

    public static ModelIdentity of(ModelEdition edition) {
        return new ModelIdentity(Scope.UNIVERSALITY, edition);
    }

    public static ModelIdentity of(Scope scope, ModelEdition edition) {
        return new ModelIdentity(scope, edition);
    }

    public static ModelIdentity epoch() {
        return epoch(Scope.UNIVERSALITY);
    }

    public static ModelIdentity epoch(Scope scope) {
        return new ModelIdentity(scope, ModelEdition.epoch());
    }

    public ModelIdentity next() {
        return new ModelIdentity(scope, edition.next());
    }
}
