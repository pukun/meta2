package io.odsp.domain.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class ModelVersionConverter implements AttributeConverter<ModelVersion, String> {
    @Override
    public String convertToDatabaseColumn(ModelVersion modelVersion) {
        return modelVersion == null ? null : modelVersion.code() ;
    }

    @Override
    public ModelVersion convertToEntityAttribute(String modelVersion) {
        return modelVersion == null ? null : new ModelVersion(modelVersion);
    }
}
