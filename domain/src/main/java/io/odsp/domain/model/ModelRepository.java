package io.odsp.domain.model;

import io.odsp.domain.common.Scope;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface ModelRepository extends PagingAndSortingRepository<Model, Long>, JpaSpecificationExecutor<Model> {

    default Optional<Model> latestUniversalModel() {
        return latestModelInScope(Scope.UNIVERSALITY);
    }

    @Query("select m from Model m where m.isLatest = true and m.scope = :scope")
    Optional<Model> latestModelInScope(@Param("scope") Scope scope);

    @Query("select m from Model m where m.scope = :#{#identity.scope} and m.edition = :#{#identity.edition}")
    Optional<Model> findModelByIdentity(@Param("identity") ModelIdentity identity);
}
