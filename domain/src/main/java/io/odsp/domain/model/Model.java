package io.odsp.domain.model;

import io.odsp.domain.common.Mutable;
import io.odsp.domain.common.Scope;
import io.odsp.domain.meta.MetaObject;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Optional;

@Accessors(fluent = true)
@Getter
@Setter
@Entity
@Table(name = "t_model")
public class Model extends Mutable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "modelGenerator")
    @SequenceGenerator(name = "modelGenerator", sequenceName = "seq_model", allocationSize = 1)
    private long id;

    @Column(nullable = false, updatable = false)
    @Enumerated(EnumType.STRING)
    Scope scope;

    @Column(nullable = false, updatable = false)
    @Convert(converter = ModelEditionConverter.class)
    ModelEdition edition;

    @Column(name = "latest_flag")
    boolean isLatest;

    @Convert(converter = ModelVersionConverter.class)
    ModelVersion version;

    @OneToOne
    @JoinColumn(name = "reference_model_id")
    Model reference;


    // region domain
    public ModelIdentity identityOf() {
        return new ModelIdentity(scope, edition);
    }

    public boolean canBeUpgradedTo(Model upstream) {
        return Optional.ofNullable(this.reference)
                .map(Model::edition)
                .or(() -> Optional.of(ModelEdition.epoch()))
                .map(upstream.edition()::isLaterThan)
                .orElse(false);
    }

    public ModelEntry entryOf(MetaObject metaObject) {
        return ModelEntry.from(this, metaObject);
    }

    // endregion

}
