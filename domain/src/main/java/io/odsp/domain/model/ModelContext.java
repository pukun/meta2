package io.odsp.domain.model;

import io.odsp.domain.common.Scope;
import io.odsp.domain.meta.MetaObject;
import io.odsp.domain.meta.MetaObjectCode;

import java.util.List;
import java.util.Optional;

public record ModelContext(ModelFactory factory, ModelRepository repository, ModelEntryRepository entryRepository) {
    public Optional<Model> findModelByIdentity(ModelIdentity modelIdentity) {
        return repository.findModelByIdentity(modelIdentity);
    }

    public Model sinceEpoch() {
        return factory.sinceEpoch();
    }

    public Model sinceEpoch(Scope scope, Model upstreamModel) {
        return factory.sinceEpoch(scope, upstreamModel);
    }

    public Model successorOf(Model model) {
        return factory.successorOf(model);
    }

    public Optional<Model> latestUniversalModel() {
        return repository.latestUniversalModel();
    }

    public Optional<Model> latestModelInScope(Scope scope) {
        return repository.latestModelInScope(scope);
    }

    public Optional<Model> latestModel(Scope scope) {
        return repository.latestModelInScope(scope);
    }

    public Model add(Model model) {
        return repository.save(model);
    }

    public ModelEntry add(ModelEntry modelEntry) {
        return entryRepository.save(modelEntry);
    }

    public Optional<MetaObject> findObjectInModel(ModelIdentity modelIdentity, MetaObjectCode code) {
        return entryRepository.findEntryByCode(modelIdentity, code).map(ModelEntry::object);
    }

    public List<MetaObject> findAllObjectsInModel(Model model) {
        return entryRepository.findAllObjectsInModel(model);
    }

    public List<MetaObject> findActiveObjectsInModel(Model model) {
        return entryRepository.findActiveObjectsInModel(model);
    }

    public List<MetaObject> findActiveUniversalObjectsInModel(Model model) {
        return entryRepository.findActiveUniversalObjectsInModel(model);
    }
}

