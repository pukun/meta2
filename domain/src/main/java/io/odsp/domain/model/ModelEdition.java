package io.odsp.domain.model;

public record ModelEdition(int edition) implements Comparable<ModelEdition> {

    public ModelEdition {
        if (edition < 0) throw new IllegalArgumentException(String.format("Given edition(%d) is negative", edition));
    }

    public static ModelEdition epoch() {
        return new ModelEdition(0);
    }

    public static ModelEdition of(int edition) {
        return new ModelEdition(edition);
    }

    public ModelEdition next() {
        return new ModelEdition(edition + 1);
    }

    public boolean isLaterThan(ModelEdition another) {
        return this.compareTo(another) > 0;
    }

    @Override
    public int compareTo(ModelEdition o) {
        return this.edition - o.edition;
    }
}
