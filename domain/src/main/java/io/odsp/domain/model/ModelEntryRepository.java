package io.odsp.domain.model;

import io.odsp.domain.meta.MetaObject;
import io.odsp.domain.meta.MetaObjectCode;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface ModelEntryRepository extends PagingAndSortingRepository<ModelEntry, ModelEntryId> {

    default Optional<ModelEntry> findEntryByCode(@Param("model") Model model, @Param("code") MetaObjectCode code) {
        return findEntryByCode(model.identityOf(), code);
    }

    @Query("select me.object from ModelEntry me where me.model = :model")
    List<MetaObject> findAllObjectsInModel(@Param("model") Model model);

    @Query("" +
            "select me from ModelEntry me where " +
            "me.model.scope = :#{#modelIdentity.scope} and me.model.edition = :#{#modelIdentity.edition} " +
            "and me.object.code = :code"
    )
    Optional<ModelEntry> findEntryByCode(@Param("modelIdentity") ModelIdentity modelIdentity, @Param("code") MetaObjectCode code);


    @Query("select me.object from ModelEntry me where me.model = :model and me.object.isRemoved = false")
    List<MetaObject> findActiveObjectsInModel(@Param("model") Model model);

    @Query("""
            select me.object from ModelEntry me where
            me.model = :model and me.object.isRemoved = false
            and me.object.isUniversal = true
            """
    )
    List<MetaObject> findActiveUniversalObjectsInModel(@Param("model") Model model);

}
