package io.odsp.domain.model;

public record ModelVersion(String code) {
    public static ModelVersion of(String no) {
        return new ModelVersion(no);
    }
}
