package io.odsp.domain.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Getter
@Setter
@EqualsAndHashCode
@Embeddable
class ModelEntryId implements Serializable {
    @Column(name = "model_id")
    private Long modelId;

    @Column(name = "object_id")
    private Long objectId;

}
