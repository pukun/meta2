package io.odsp.domain.model;

import io.odsp.domain.common.Immutable;
import io.odsp.domain.meta.MetaObject;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Accessors(fluent = true, chain = true)
@Getter
@Setter
@Entity
@Table(name = "t_model_entry")
public class ModelEntry extends Immutable {
    @EmbeddedId
    private ModelEntryId id;

    @MapsId("modelId")
    @ManyToOne(optional = false)
    private Model model;

    @MapsId("objectId")
    @ManyToOne(optional = false)
    private MetaObject object;


    public static ModelEntry from(Model model, MetaObject object) {
        ModelEntry modelEntry = new ModelEntry();

        modelEntry.id = new ModelEntryId();

        modelEntry.model = model;
        modelEntry.object = object;

        return modelEntry;
    }

}
