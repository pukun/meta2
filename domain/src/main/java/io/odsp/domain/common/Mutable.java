package io.odsp.domain.common;

import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;
import java.util.Date;

@MappedSuperclass
public class Mutable extends Immutable {

    @UpdateTimestamp
    @Column(name = "last_updated_time")
    private Date lastUpdatedTime;

    @Column(name = "change_sequence", nullable = false)
    @Version
    private int changeSequence;


    public Date getLastUpdatedTime() {
        return lastUpdatedTime;
    }

    public void setLastUpdatedTime(Date lastUpdatedTime) {
        this.lastUpdatedTime = lastUpdatedTime;
    }

    public int getChangeSequence() {
        return changeSequence;
    }

    public void setChangeSequence(int changeSequence) {
        this.changeSequence = changeSequence;
    }
}
