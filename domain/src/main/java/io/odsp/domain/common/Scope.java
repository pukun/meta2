package io.odsp.domain.common;

public enum Scope {
    UNIVERSALITY,
    JX,
    SH,
    ;

    public boolean isUniversal() {
        return this == UNIVERSALITY;
    }

    public boolean isProprietary() {
        return !isUniversal();
    }
}
