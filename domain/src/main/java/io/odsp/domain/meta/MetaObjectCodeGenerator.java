package io.odsp.domain.meta;

public interface MetaObjectCodeGenerator {
    MetaObjectCode code(MetaObjectType objectType);

    default MetaObjectCode codeOfEntity() {
        return code(MetaObjectType.ENTITY);
    }

    default MetaObjectCode codeOfAssociation() {
        return code(MetaObjectType.ASSOCIATION);
    }

}
