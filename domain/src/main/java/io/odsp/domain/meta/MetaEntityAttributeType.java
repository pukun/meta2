package io.odsp.domain.meta;

public enum MetaEntityAttributeType {
    NUMBER,
    LITERAL,
}
