package io.odsp.domain.meta;

public record MetaAssociationModificationSpec(
        String label,
        MetaObjectCode fromEntity, String fromRole, Multiplicity fromMultiplicity,
        MetaObjectCode toEntity, String toRole, Multiplicity toMultiplicity
) implements MetaObjectModificationSpec {
}