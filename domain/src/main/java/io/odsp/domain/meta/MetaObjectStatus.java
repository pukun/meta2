package io.odsp.domain.meta;

public enum MetaObjectStatus {
    ACTIVE,
    OBSOLETE,
}
