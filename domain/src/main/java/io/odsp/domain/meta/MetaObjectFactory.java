package io.odsp.domain.meta;

public record MetaObjectFactory(MetaObjectCodeGenerator codeGenerator) {

    public MetaObject create(MetaEntityCreationSpec spec) {
        return MetaEntity.from(codeGenerator.codeOfEntity(), spec);
    }

    public MetaObject create(MetaAssociationCreationSpec spec) {
        return MetaAssociation.from(codeGenerator.codeOfAssociation(), spec);
    }
}
