package io.odsp.domain.meta;

public enum Multiplicity {
    ZERO_OR_ONE,
    EXACT_ONE,
    ZERO_TO_MANY,
    AT_LEAST_ONE,
}
