package io.odsp.domain.meta;

public record MetaObjectContext(MetaObjectFactory factory, MetaObjectRepository repository) {

    public MetaObject create(MetaObjectCreationSpec spec) {
        if (spec instanceof MetaEntityCreationSpec entitySpec) return factory.create(entitySpec);
        else if (spec instanceof MetaAssociationCreationSpec associationSpec)
            return factory.create(associationSpec);
        else return null;
    }
}
