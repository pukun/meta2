package io.odsp.domain.meta;

import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.*;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "t_meta_entity")
@PrimaryKeyJoinColumn
public class MetaEntity extends MetaObject {

    @ElementCollection
    @CollectionTable(name = "t_meta_entity_attribute", joinColumns = {@JoinColumn(name = "entity_id")})
    @OrderBy("sequence")
    private Set<MetaEntityAttribute> attributes = new LinkedHashSet<>();

    //region domain
    public static MetaObject from(MetaObjectCode code, MetaEntityCreationSpec spec) {
        return new MetaEntity()
                .withAttributes(spec.attributes())
                .code(code)
                .isUniversal(spec.isUniversal())
                .label(spec.label())
                .edition(MetaObjectEdition.InitialEdition);
    }

    @Override
    MetaObject instance() {
        return new MetaEntity()
                .withAttributes(this.attributes);
    }

    @Override
    public MetaObjectType typeOf() {
        return MetaObjectType.ENTITY;
    }

    @Override
    public MetaObject modify(MetaObjectModificationSpec spec) {
        if (spec instanceof MetaEntityModificationSpec modification) {
            return withAttributes(modification.attributes())
                    .label(modification.label());
        } else return this;
    }

    @Override
    public MetaObjectModificationSpec modificationSpecOf() {
        return new MetaEntityModificationSpec(label, attributes.stream().toList());
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("code", code)
                .append("label", label)
                .append("edition", edition)
                .append("attributes", attributes)
                .toString();
    }

    public MetaEntity withAttributes(Collection<MetaEntityAttribute> attributes) {
        this.attributes.clear();

        this.attributes.addAll(attributes);

        int sequence = 0;
        for (MetaEntityAttribute attr : attributes) {
            attr.setSequence(sequence++);
        }

        return this;
    }

    //region accessor

    public List<MetaEntityAttribute> getAttributes() {
        return attributes.stream().toList();
    }


    //endregion
}
