package io.odsp.domain.meta;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Embeddable
@Data
@EqualsAndHashCode(of = "label")
@ToString(exclude={"sequence"})
public class MetaEntityAttribute {
    @Column(nullable = false, updatable = false)
    private int sequence;

    @Column(nullable = false)
    private String label;

    @Enumerated(EnumType.STRING)
    private MetaEntityAttributeType type;


    //region domain

    public static MetaEntityAttribute of(int sequence, String label, MetaEntityAttributeType type) {
       MetaEntityAttribute attribute = new MetaEntityAttribute() ;
       attribute.sequence = sequence;
       attribute.label = label;
       attribute.type = type;

       return attribute;
    }


    //endregion
}
