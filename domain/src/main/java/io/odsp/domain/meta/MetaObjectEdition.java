package io.odsp.domain.meta;

public record MetaObjectEdition(int edition) {

    public final static MetaObjectEdition InitialEdition = new MetaObjectEdition(0);

    public MetaObjectEdition {
        if (edition < 0) throw new IllegalArgumentException(String.format("Given edition(%d) is negative", edition));
    }

    public static MetaObjectEdition of(int edition) {
        return new MetaObjectEdition(edition);
    }

    public MetaObjectEdition next() {
        return new MetaObjectEdition(edition + 1);
    }
}
