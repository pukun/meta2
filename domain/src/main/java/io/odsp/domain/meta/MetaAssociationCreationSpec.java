package io.odsp.domain.meta;

import io.odsp.domain.common.Scope;

public record MetaAssociationCreationSpec(
        boolean isUniversal, String label,
        MetaObjectCode fromEntity, String fromRole, Multiplicity fromMultiplicity,
        MetaObjectCode toEntity, String toRole, Multiplicity toMultiplicity
) implements MetaObjectCreationSpec {
}