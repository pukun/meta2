package io.odsp.domain.meta;

public record MetaObjectCode(String code) {

    public static MetaObjectCode of(String code) {
        return new MetaObjectCode(code);
    }
}
