package io.odsp.domain.meta;

import io.odsp.domain.common.Mutable;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Objects;

@Accessors(fluent = true, chain = true)
@Getter
@Setter
@Entity
@Table(name = "t_meta_object")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class MetaObject extends Mutable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "objectGenerator")
    @SequenceGenerator(name = "objectGenerator", sequenceName = "seq_meta")
    Long id;

    @Column(nullable = false, updatable = false)
    @Convert(converter = MetaObjectCodeConverter.class)
    MetaObjectCode code;

    @Column(name = "universal_flag", nullable = false, updatable = false)
    boolean isUniversal;

    @Column(nullable = false)
    String label;

    @Column(nullable = false, updatable = false)
    @Convert(converter = MetaObjectEditionConverter.class)
    MetaObjectEdition edition;

    @Column(name = "is_removed")
    boolean isRemoved;

    // region domain

    public final MetaObject successorOf() {
        return instance()
                .code(code)
                .isUniversal(isUniversal)
                .label(label)
                .edition(edition.next())
                .isRemoved(false);
    }

    abstract MetaObject instance();

    public abstract MetaObjectType typeOf();

    public abstract MetaObject modify(MetaObjectModificationSpec spec);

    public abstract MetaObjectModificationSpec modificationSpecOf();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MetaObject mo)) return false;
        return Objects.equals(code, mo.code()) && Objects.equals(edition, mo.edition());
    }

    @Override
    public int hashCode() {
        return Objects.hash(code(), edition());
    }

    //endregion
}