package io.odsp.domain.meta;

import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface MetaObjectRepository extends PagingAndSortingRepository<MetaObject, String> {

    List<MetaObject> findByCode(MetaObjectCode code);
}
