package io.odsp.domain.meta;

public enum MetaObjectType {
    ENTITY("e"),
    ASSOCIATION("a");

    private final String tag;

    MetaObjectType(String tag) {
        this.tag = tag;
    }

    public String tag() {
        return tag;
    }
}
