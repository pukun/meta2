package io.odsp.domain.meta;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class MetaObjectEditionConverter implements AttributeConverter<MetaObjectEdition, Integer> {
    @Override
    public Integer convertToDatabaseColumn(MetaObjectEdition metaObjectEdition) {
        return metaObjectEdition == null ? null : metaObjectEdition.edition();
    }

    @Override
    public MetaObjectEdition convertToEntityAttribute(Integer entityEdition) {
        return entityEdition == null ? null : new MetaObjectEdition(entityEdition);
    }
}
