package io.odsp.domain.meta;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.*;

@Accessors(fluent = true)
@Getter
@Setter
@Entity
@Table(name = "t_meta_association")
@PrimaryKeyJoinColumn
public class MetaAssociation extends MetaObject {

    @Column(name = "from_entity_code", nullable = false, updatable = false)
    @Convert(converter = MetaObjectCodeConverter.class)
    private MetaObjectCode fromEntity;

    @Column(name = "from_role")
    private String fromRole;

    @Column(name = "from_multiplicity")
    @Enumerated(EnumType.STRING)
    private Multiplicity fromMultiplicity;

    @Column(name = "to_entity_code", nullable = false, updatable = false)
    @Convert(converter = MetaObjectCodeConverter.class)
    private MetaObjectCode toEntity;

    @Column(name = "to_role")
    private String toRole;

    @Column(name = "to_multiplicity")
    @Enumerated(EnumType.STRING)
    private Multiplicity toMultiplicity;


    //region domain

    public static MetaObject from(MetaObjectCode code, MetaAssociationCreationSpec spec) {
        return new MetaAssociation()
                .fromEntity(spec.fromEntity())
                .fromRole(spec.fromRole())
                .fromMultiplicity(spec.fromMultiplicity())
                .toEntity(spec.toEntity())
                .toRole(spec.toRole())
                .toMultiplicity(spec.toMultiplicity())
                .code(code)
                .isUniversal(spec.isUniversal())
                .label(spec.label())
                .edition(MetaObjectEdition.InitialEdition);
    }

    @Override
    MetaObject instance() {
        return new MetaAssociation()
                .fromEntity(this.fromEntity)
                .fromRole(fromRole)
                .fromMultiplicity(this.fromMultiplicity)
                .toEntity(toEntity)
                .toRole(toRole)
                .toMultiplicity(toMultiplicity);
    }

    @Override
    public MetaObjectType typeOf() {
        return MetaObjectType.ASSOCIATION;
    }

    @Override
    public MetaObject modify(MetaObjectModificationSpec spec) {
        if (spec instanceof MetaAssociationModificationSpec modification) {
            return fromEntity(modification.fromEntity())
                    .fromRole(modification.fromRole())
                    .fromMultiplicity(modification.fromMultiplicity())
                    .toEntity(modification.toEntity())
                    .toRole(modification.toRole())
                    .toMultiplicity(modification.toMultiplicity())
                    .label(modification.label());
        } else return this;
    }

    @Override
    public MetaObjectModificationSpec modificationSpecOf() {
        return new MetaAssociationModificationSpec(
                label, fromEntity, fromRole, fromMultiplicity,
                toEntity, toRole, toMultiplicity
        );
    }

    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("code", code)
                .append("edition", edition)
                .append("label", label)
                .append("fromEntity", fromEntity)
                .append("fromRole", fromRole)
                .append("fromMultiplicity", fromMultiplicity)
                .append("toEntity", toEntity)
                .append("toRole", toRole)
                .append("toMultiplicity", toMultiplicity)
                .toString();
    }
    //endregion

    //region accessor


    public MetaObjectCode getFromEntity() {
        return fromEntity;
    }

    public String getFromRole() {
        return fromRole;
    }

    public void setFromRole(String fromRole) {
        this.fromRole = fromRole;
    }

    public Multiplicity getFromMultiplicity() {
        return fromMultiplicity;
    }

    public void setFromMultiplicity(Multiplicity fromMultiplicity) {
        this.fromMultiplicity = fromMultiplicity;
    }

    public MetaObjectCode getToEntity() {
        return toEntity;
    }

    public String getToRole() {
        return toRole;
    }

    public void setToRole(String toRole) {
        this.toRole = toRole;
    }

    public Multiplicity getToMultiplicity() {
        return toMultiplicity;
    }

    public void setToMultiplicity(Multiplicity toMultiplicity) {
        this.toMultiplicity = toMultiplicity;
    }


    public void setFromEntity(MetaObjectCode fromEntity) {
        this.fromEntity = fromEntity;
    }

    public void setToEntity(MetaObjectCode toEntity) {
        this.toEntity = toEntity;
    }
//endregion
}
