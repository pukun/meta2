package io.odsp.domain.meta;

import java.util.List;

public record MetaEntityModificationSpec(String label, List<MetaEntityAttribute> attributes) implements MetaObjectModificationSpec{
}
