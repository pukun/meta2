package io.odsp.domain.meta;

import io.odsp.domain.common.Scope;

import java.util.List;

public record MetaEntityCreationSpec(
        boolean isUniversal, String label,
        List<MetaEntityAttribute> attributes
) implements MetaObjectCreationSpec {
}
