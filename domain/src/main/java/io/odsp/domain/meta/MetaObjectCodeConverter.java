package io.odsp.domain.meta;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class MetaObjectCodeConverter implements AttributeConverter<MetaObjectCode, String> {
    @Override
    public String convertToDatabaseColumn(MetaObjectCode metaObjectCode) {
        return metaObjectCode == null ? null : metaObjectCode.code() ;
    }

    @Override
    public MetaObjectCode convertToEntityAttribute(String entityCode) {
        return entityCode == null ? null : new MetaObjectCode(entityCode);
    }
}
