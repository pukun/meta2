package io.odsp.domain.revision;

import io.odsp.domain.common.Mutable;
import io.odsp.domain.meta.MetaObject;
import io.odsp.domain.meta.MetaObjectModificationSpec;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Accessors(fluent = true, chain = true)
@Getter
@Setter
@Entity
@Table(name = "t_revision_change")
public class Change extends Mutable {
    @Id
    @GeneratedValue(generator = "changeGenerator")
    @SequenceGenerator(name = "changeGenerator", sequenceName = "seq_change")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "revision_id")
    private Revision revision;

    @ManyToOne
    @JoinColumn(name = "prototype_object_id")
    private MetaObject prototype;

    @ManyToOne
    @JoinColumn(name = "reference_object_id")
    private MetaObject reference;

    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    @JoinColumn(name = "product_object_id")
    private MetaObject product;

    @Column(name = "conflict_flag")
    private boolean isConflicting;


    public Change revise(MetaObjectModificationSpec spec) {
        this.product.modify(spec);
        return this;
    }

    public Change resolve() {
        this.isConflicting = false;
        return this;
    }

    public Change remove() {
        product.isRemoved(true);
        return this;
    }
}
