package io.odsp.domain.revision;

public enum RevisionStatus {
    REVISING,
    INSPECTING,
    APPLIED,
    ARCHIVED,
}
