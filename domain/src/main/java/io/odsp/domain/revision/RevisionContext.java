package io.odsp.domain.revision;

import io.odsp.domain.common.Scope;
import io.odsp.domain.meta.MetaObject;
import io.odsp.domain.meta.MetaObjectCode;
import io.odsp.domain.meta.MetaObjectContext;
import io.odsp.domain.meta.MetaObjectCreationSpec;
import io.odsp.domain.model.Model;
import io.odsp.domain.model.ModelContext;
import io.odsp.domain.model.ModelEdition;

import java.util.Optional;

public record RevisionContext(
        MetaObjectContext objectContext,
        ModelContext modelContext,
        RevisionEntryRepository entryRepository,
        ChangeRepository changeRepository
) {

    //region model
    public Optional<Model> latestUniversalModel() {
        return modelContext.latestUniversalModel();
    }

    Optional<Model> prototypeOf(Revision revision) {
        return modelContext.findModelByIdentity(revision.identityOfPrototypeModel());
    }

    Model successorOf(Model model) {
        return modelContext.successorOf(model);
    }

    Model successorOf() {
        return modelContext.sinceEpoch();
    }

    Model successorOf(Scope scope, Model usedUniversalModel) {
        return modelContext.sinceEpoch(scope, usedUniversalModel);
    }

    Model save(Model model) {
        return modelContext.repository().save(model);
    }

    ModelEdition editionOfLatestModel(Revision revision) {
        return modelContext.latestModel(revision.scope())
                .map(Model::edition)
                .orElse(ModelEdition.epoch());
    }
    //endregion


    //region object
    MetaObject createNewObject(MetaObjectCreationSpec objectSpec) {
        return objectContext.create(objectSpec);
    }

    Optional<MetaObject> prototypeOf(MetaObjectCode code, Revision revision) {
        return modelContext.findObjectInModel(revision.identityOfPrototypeModel(), code);
    }
    //endregion

    //region change
    Optional<Change> changeOf(MetaObjectCode code, Revision revision) {
        return changeRepository.findByCode(revision, code);
    }

    public Change addChange(Change change) {
        return changeRepository.save(change);
    }

    boolean removeChange(MetaObjectCode code, Revision revision) {
        return changeRepository.remove(revision, code) > 0;
    }

    int numberOfConflictingChanges(Revision revision) {
        return changeRepository.numberOfConflictingChanges(revision);
    }
    //endregion


    //region revision
    int rebase(ModelEdition toEdition, Revision revision) {
        return changeRepository.rebase(revision, toEdition);
    }

    void apply(Revision revision) {
        changeRepository.apply(revision);
    }
    //endregion

}
