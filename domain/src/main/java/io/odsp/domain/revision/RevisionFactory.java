package io.odsp.domain.revision;

import io.odsp.domain.common.Scope;
import io.odsp.domain.meta.MetaObject;
import io.odsp.domain.meta.MetaObjectCode;
import io.odsp.domain.model.Model;
import io.odsp.domain.model.ModelContext;
import io.odsp.domain.model.ModelEdition;
import io.odsp.domain.model.ModelIdentity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

public record RevisionFactory(RevisionRepository repository, RevisionContext context) {
    private static final Logger logger = LoggerFactory.getLogger(RevisionFactory.class);


    //region create revision for latest model
    public RevisionCreation.Ok revise() {
        return reviseUniversalModel();
    }

    public RevisionCreation.Ok revise(Scope scope) {
        if (scope == null || scope == Scope.UNIVERSALITY) return reviseUniversalModel();
        else return reviseProprietaryModel(scope);
    }

    private ModelContext modelContext() {
        return context.modelContext();
    }

    private RevisionCreation.Ok reviseUniversalModel() {
        return modelContext().latestUniversalModel()
                .map(Revision::new)
                .map(RevisionCreation::ok)
                .orElseGet(this::reviseEpochModel);
    }

    private RevisionCreation.Ok reviseProprietaryModel(Scope scope) {
        return modelContext().latestModelInScope(scope)
                .map(Revision::new)
                .map(RevisionCreation::ok)
                .orElseGet(() -> reviseEpochModel(scope));
    }

    private RevisionCreation.Ok reviseEpochModel() {
        return RevisionCreation.ok(new Revision(ModelIdentity.epoch()));
    }

    private RevisionCreation.Ok reviseEpochModel(Scope scope) {
        return modelContext().latestUniversalModel()
                .map(universalModel -> reviseEpochModel(scope, universalModel))
                .orElse(RevisionCreation.ok(new Revision(ModelIdentity.epoch(scope), Optional.empty())));
    }

    public RevisionCreation.Ok reviseEpochModel(Scope scope, Model universalModel) {
        Revision revision = new Revision(ModelIdentity.epoch(scope), Optional.of(universalModel));

        return RevisionCreation.ok(
                revision,
                modelContext().findActiveObjectsInModel(universalModel).stream()
                        .map(revision::changeOfReference)
                        .toList()
        );
    }
    //endregion


    //region create revision for updating upstream model
    public RevisionCreation upgrade(Scope scope, ModelEdition editionOfUniversalModel) {
        if (scope == null || scope == Scope.UNIVERSALITY) return RevisionCreation.notEligibleScope();

        Optional<Model> pModel = modelContext().latestModelInScope(scope);
        if (pModel.isEmpty()) return RevisionCreation.modelNotFoundInScope(scope);

        Optional<Model> uModel = modelContext().findModelByIdentity(ModelIdentity.of(editionOfUniversalModel));
        if (uModel.isEmpty()) return RevisionCreation.noSuchReferenceModel(editionOfUniversalModel);

        Model proprietaryModel = pModel.get();
        Model universalModel = uModel.get();
        if (!proprietaryModel.canBeUpgradedTo(universalModel))
            return RevisionCreation.notLaterReferenceModel(proprietaryModel.edition(), universalModel.edition());

        return upgrade(proprietaryModel, universalModel);

    }

    private RevisionCreation upgrade(Model proprietaryModel, Model universalModel) {
        Revision revision = updateOf(proprietaryModel, universalModel);
        ModelDelta modelDelta = generateDeltaBetweenModels(proprietaryModel, universalModel);
        List<Change> changes = generateChangesForUpgrade(revision, modelDelta);

        return RevisionCreation.ok(revision, changes);
    }

    protected record ModelDelta(
            List<MetaObject> activeReferencedObjects,
            List<MetaObject> activeReferencingObjects,
            List<MetaObject> removedReferencedObjects,
            List<MetaObject> removedReferencingObjects
    ) {
    }

    Revision updateOf(Model proprietaryModel, Model universalModel) {
        return new Revision(proprietaryModel.identityOf(), Optional.of(universalModel));
    }

    ModelDelta generateDeltaBetweenModels(Model proprietaryModel, Model universalModel) {
        return new ModelDelta(
                modelContext().findActiveObjectsInModel(universalModel),
                modelContext().findActiveUniversalObjectsInModel(proprietaryModel),
                context.entryRepository().findRemovedReferencedObjects(proprietaryModel, universalModel),
                context.entryRepository().findRemovedReferencingObjects(proprietaryModel, universalModel)
        );
    }

    List<Change> generateChangesForUpgrade(Revision revision, ModelDelta modelDelta) {
        Map<MetaObjectCode, MetaObject> activeReferencedObjects = modelDelta.activeReferencedObjects.stream()
                .collect(Collectors.toMap(MetaObject::code, Function.identity()));

        Map<MetaObjectCode, MetaObject> activeReferencingObjects = modelDelta.activeReferencingObjects.stream()
                .collect(Collectors.toMap(MetaObject::code, Function.identity()));

        Map<MetaObjectCode, MetaObject> removedReferencedObjects = modelDelta.removedReferencedObjects.stream()
                .collect(Collectors.toMap(MetaObject::code, Function.identity()));

        Map<MetaObjectCode, MetaObject> removedReferencingObjects = modelDelta.removedReferencingObjects.stream()
                .collect(Collectors.toMap(MetaObject::code, Function.identity()));

        List<Change> changes = new ArrayList<>();

        for (MetaObjectCode code : activeReferencedObjects.keySet()) {
            MetaObject reference = activeReferencedObjects.get(code);

            if (activeReferencingObjects.containsKey(code))
                changes.add(revision.changeOfReference(activeReferencingObjects.get(code), reference));
            else if (removedReferencingObjects.containsKey(code))
                changes.add(revision.changeOfReference(removedReferencingObjects.get(code), reference));
            else
                changes.add(revision.changeOfReference(reference));
        }

        for (MetaObjectCode code : activeReferencingObjects.keySet()) {
            MetaObject prototype = activeReferencingObjects.get(code);
            Optional.ofNullable(removedReferencedObjects.get(code))
                    .ifPresent(reference -> changes.add(revision.changeOfReference(prototype, reference)));
        }

        return changes;
    }

    //endregion
}
