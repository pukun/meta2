package io.odsp.domain.revision;

import io.odsp.domain.meta.MetaObjectCode;
import io.odsp.domain.model.ModelEdition;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface ChangeRepository extends PagingAndSortingRepository<Change, Long> {

    @Query("select d from Change d where d.revision = :revision and code = :code")
    Optional<Change> findByCode(@Param("revision") Revision revision, @Param("code") MetaObjectCode code);

    @Query("select r from Change r where r.revision = :revision ")
    List<Change> findAllChangesInRevision(@Param("revision") Revision revision);

    @Modifying
    @Query("delete from Change c where c.revision = :revision and code = :code ")
    int remove(@Param("revision") Revision revision, @Param("code") MetaObjectCode code);

    @Modifying
    @Query(
            value = """
                    update t_revision_change change set
                    prototype_object_id = prototype.id,
                    conflict_flag = true
                    from t_meta_object as product,
                    (
                        select h.code as code, max(h.id) as id from
                        t_revision r
                        join t_revision_change c on r.id = c.revision_id
                        join t_meta_object h on c.product_object_id = h.id
                        where
                        r.status = 'APPLIED'
                        and r.scope = :#{#revision.scope().name()}
                        and r.edition >= :#{#revision.edition().edition()}
                        and r.edition < :#{#edition.edition()}
                        group by h.code
                    ) as prototype
                    where
                    change.revision_id = :#{#revision.id()}
                    and product_object_id = product.id
                    and product.code = prototype.code
                    """,
            nativeQuery = true
    )
    int rebase(@Param("revision") Revision revision, @Param("edition") ModelEdition edition);


    @Query("select count(r) from Change r where r.isConflicting = true and r.revision = :revision ")
    int numberOfConflictingChanges(@Param("revision") Revision revision);

    @Modifying
    @Query(
            value = """
                    insert into t_model_entry (model_id, object_id)
                    select :#{#revision.productModel().id()}, object_id
                    from t_model_entry join t_model m on model_id = m.id
                    join t_meta_object o on object_id = o.id
                    where
                    o.is_removed = false
                    and m.scope = :#{#revision.scope().name()} and m.edition = :#{#revision.edition().edition()}
                    and not exists (select 1 from t_revision_change where o.id = prototype_object_id and revision_id = :#{#revision.id()})
                    ;
                                        
                    insert into t_model_entry (model_id, object_id)
                    select :#{#revision.productModel().id()}, change.product_object_id
                    from t_revision_change change join t_meta_object product on product_object_id = product.id
                    where
                    (change.prototype_object_id is not null or product.is_removed = false)
                    and change.revision_id = :#{#revision.id()}
                    """,
            nativeQuery = true
    )
    int apply(@Param("revision") Revision revision);

}
