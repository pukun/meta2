package io.odsp.domain.revision;

public interface ApplyingFailure {

    String detail();

    static WithConflict withConflict() {
        return new WithConflict("Can not merge revision with conflicts");
    }

    static NotAllowed notAllowed(Revision revision) {
        return new NotAllowed(String.format("Can not merge revision with status:{%s}", revision.status()));
    }

    static NotTargetTheLatestModel notTargetTheLatestModel(Revision revision) {
        return new NotTargetTheLatestModel(String.format("Revised model(%d) is not the latest one", revision.edition().edition()));
    }

    static NotFound notFound(Long revisionId) {
        return new NotFound(String.format("Can not find revision: %d", revisionId));
    }

    static NoPrototype noPrototype(Revision revision) {
        return new NoPrototype(String.format("Can not find prototype(scope=%s, edition=%d) of revision", revision.scope(),
                revision.edition().edition()));
    }

    record NotFound(String detail) implements ApplyingFailure {
    }

    record WithConflict(String detail) implements ApplyingFailure {
    }

    record NotAllowed(String detail) implements ApplyingFailure {
    }

    record NotTargetTheLatestModel(String detail) implements ApplyingFailure {
    }

    record NoPrototype(String detail) implements ApplyingFailure {

    }

    record UnexpectedFailure(String detail) implements ApplyingFailure {
        public UnexpectedFailure(Exception e) {
            this(e.getMessage());
        }
    }
}
