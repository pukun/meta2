package io.odsp.domain.revision;

import io.odsp.domain.common.Mutable;
import io.odsp.domain.common.Scope;
import io.odsp.domain.meta.*;
import io.odsp.domain.model.Model;
import io.odsp.domain.model.ModelEdition;
import io.odsp.domain.model.ModelEditionConverter;
import io.odsp.domain.model.ModelIdentity;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.tuple.Pair;

import javax.persistence.*;
import java.util.Objects;
import java.util.Optional;

@Accessors(fluent = true)
@Getter
@Setter
@Entity
@Table(name = "t_revision")
public class Revision extends Mutable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "revisionGenerator")
    @SequenceGenerator(name = "revisionGenerator", sequenceName = "seq_revision", allocationSize = 1)
    private Long id;

    @Column(name = "scope", nullable = false, updatable = false)
    @Enumerated(EnumType.STRING)
    private Scope scope;

    @Column(name = "edition", nullable = false, updatable = false)
    @Convert(converter = ModelEditionConverter.class)
    private ModelEdition edition;

    @ManyToOne
    @JoinColumn(name = "reference_model_id")
    private Model referenceModel;

    @Enumerated(EnumType.STRING)
    private RevisionStatus status;

    private int commit;

    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "product_model_id")
    private Model productModel;


    //region constructor

    public Revision() {
    }

    Revision(Model revisedModel) {
        this(revisedModel.identityOf(), Optional.empty());
    }

    Revision(ModelIdentity revisedModel) {
        this(revisedModel, Optional.empty());
    }

    Revision(ModelIdentity revisedModel, Optional<Model> specifiedUniversalModel) {
        this.scope = revisedModel.scope();
        this.edition = revisedModel.edition();

        this.referenceModel = specifiedUniversalModel.orElse(null);

        this.status = RevisionStatus.REVISING;
        this.productModel = null;
    }

    //endregion

    //region change
    public ChangeResult create(MetaObjectCreationSpec spec, RevisionContext context) {
        if (!isEditable()) return new ChangeResult.RevisionNotEditable();
        this.commit++;

        return new ChangeResult.Ok(changeOfCreation(context.createNewObject(spec)));
    }

    public ChangeResult revise(MetaObjectCode code, MetaObjectModificationSpec spec, RevisionContext context) {
        if (!isEditable()) return new ChangeResult.RevisionNotEditable();
        this.commit++;

        return context.changeOf(code, this)
                .or(() -> changeOfModification(code, context))
                .map(change -> change.revise(spec))
                .map(ChangeResult::ok)
                .orElseGet(() -> ChangeResult.noSuchObject(code));
    }

    public ChangeResult remove(MetaObjectCode code, RevisionContext context) {
        if (!isEditable()) return new ChangeResult.RevisionNotEditable();
        this.commit++;

        return context.changeOf(code, this)
                .or(() -> changeOfModification(code, context))
                .map(Change::remove)
                .map(ChangeResult::ok)
                .orElseGet(() -> ChangeResult.noSuchObject(code));
    }

    public ChangeResult resolve(MetaObjectCode code, RevisionContext context) {
        if (!isEditable()) return new ChangeResult.RevisionNotEditable();
        this.commit++;

        return context.changeOf(code, this)
                .map(Change::resolve)
                .map(ChangeResult::ok)
                .orElseGet(() -> ChangeResult.noSuchObject(code));
    }

    public ChangeResult cancel(MetaObjectCode code, RevisionContext context) {
        if (!isEditable()) return new ChangeResult.RevisionNotEditable();
        this.commit++;

        context.removeChange(code, this);
        return new ChangeResult.Cancelled();
    }

    public Change changeOfCreation(MetaObject product) {
        return new Change()
                .revision(this)
                .prototype(null)
                .reference(null)
                .product(product);
    }

    private Optional<Change> changeOfModification(MetaObjectCode code, RevisionContext context) {
        return context.prototypeOf(code, this)
                .map(this::changeOfModification);
    }

    public Change changeOfModification(MetaObject prototype) {
        return new Change()
                .revision(this)
                .prototype(prototype)
                .reference(null)
                .product(prototype.successorOf());
    }

    Change changeOfReference(MetaObject prototype, MetaObject reference) {
        return new Change()
                .revision(this)
                .prototype(prototype)
                .reference(reference)
                .product(reference.successorOf().edition(prototype.edition().next()));
    }


    Change changeOfReference(MetaObject reference) {
        return new Change()
                .revision(this)
                .prototype(null)
                .reference(reference)
                .product(reference.successorOf().edition(MetaObjectEdition.InitialEdition));
    }

    //endregion

    //region rebase
    public RebaseResult rebase(RevisionContext context) {
        if (!isEditable()) return RebaseResult.notEditable();
        this.commit++;

        ModelEdition editionOfLatestModel = context.editionOfLatestModel(this);
        if (Objects.equals(editionOfLatestModel, edition)) return RebaseResult.alreadyBasedOnLatestModel();

        int rebased = context.rebase(editionOfLatestModel, this);

        this.edition = editionOfLatestModel;
        return RebaseResult.ok(rebased);
    }
    //endregion

    //region apply
    public Optional<ApplyingFailure> apply(RevisionContext context) {
        if (!isApplicable()) return Optional.of(ApplyingFailure.notAllowed(this));

        if (!isForTheLatestModel(context)) return Optional.of(ApplyingFailure.notTargetTheLatestModel(this));

        if (isThereAnyConflicts(context)) return Optional.of(ApplyingFailure.withConflict());

        Pair<Optional<Model>, Optional<Model>> prototypeAndSuccessor = successorOfPrototypeModel(context);
        if (prototypeAndSuccessor.getRight().isEmpty()) return Optional.of(ApplyingFailure.noPrototype(this));

        apply(prototypeAndSuccessor.getLeft(), prototypeAndSuccessor.getRight().get(), context);

        return Optional.empty();
    }

    private Pair<Optional<Model>, Optional<Model>> successorOfPrototypeModel(RevisionContext context) {
        if (isForEpochModel()) return scope.isUniversal() ?
                Pair.of(Optional.empty(), Optional.of(context.successorOf())) :
                Pair.of(Optional.empty(), Optional.of(context.successorOf(scope, referenceModel)));
        else return context.prototypeOf(this)
                .map(prototype -> Pair.of(Optional.of(prototype), Optional.of(context.successorOf(prototype))))
                .orElse(Pair.of(Optional.empty(), Optional.empty()));
    }

    private void apply(Optional<Model> prototypeModel, Model productModel, RevisionContext context) {
        prototypeModel.ifPresent(prototype -> context.save(prototype.isLatest(false)));

        this.productModel = context.save(productModel);

        this.status = RevisionStatus.APPLIED;

        context.apply(this);
    }
    //endregion

    //region utility
    public boolean isEditable() {
        return status == RevisionStatus.REVISING;
    }

    public boolean isApplicable() {
        return status == RevisionStatus.INSPECTING || status == RevisionStatus.REVISING;
    }

    public boolean isForTheLatestModel(RevisionContext context) {
        return context.editionOfLatestModel(this).equals(edition);
    }

    public ModelIdentity identityOfPrototypeModel() {
        return new ModelIdentity(scope, edition);
    }

    public boolean isForEpochModel() {
        return Objects.equals(edition, ModelEdition.epoch());
    }

    private boolean isThereAnyConflicts(RevisionContext context) {
        return context.numberOfConflictingChanges(this) > 0;
    }

    public Optional<Model> getReferenceModel() {
        return Optional.ofNullable(referenceModel);
    }

    //endregion
}
