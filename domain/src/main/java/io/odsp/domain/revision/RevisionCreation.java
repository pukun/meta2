package io.odsp.domain.revision;

import io.odsp.domain.common.Scope;
import io.odsp.domain.model.ModelEdition;

import java.util.Collections;
import java.util.List;

public interface RevisionCreation {
    default boolean isOk() {
        return false;
    }

    static RevisionCreation.Ok ok(Revision revision) {
        return new Ok(revision, Collections.emptyList());
    }

    static RevisionCreation.Ok ok(Revision revision, List<Change> changes) {
        return new Ok(revision, changes);
    }

    static RevisionCreation notEligibleScope() {
        return new NotProprietaryScope();
    }

    static RevisionCreation modelNotFoundInScope(Scope scope) {
        return new NoModelInScope(scope);
    }

    static RevisionCreation noSuchReferenceModel(ModelEdition referenceEdition) {
        return new NoSuchReferenceModel(referenceEdition);
    }

    static RevisionCreation notLaterReferenceModel(ModelEdition current, ModelEdition target) {
        return new AlreadyUpToDate(current, target);
    }

    record Ok(Revision revision, List<Change> changes) implements RevisionCreation {
        public boolean isOk(){
            return true;
        }
    }

    record NotProprietaryScope() implements RevisionCreation {
    }

    record NoModelInScope(Scope scope) implements RevisionCreation {
    }

    record NoSuchReferenceModel(ModelEdition referenceEdition) implements RevisionCreation {
    }

    record AlreadyUpToDate(ModelEdition currentEdition, ModelEdition targetEdition) implements RevisionCreation {
    }
}
