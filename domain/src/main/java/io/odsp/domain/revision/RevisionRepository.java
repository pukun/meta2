package io.odsp.domain.revision;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface RevisionRepository extends PagingAndSortingRepository<Revision, Long> {

}
