package io.odsp.domain.revision;

import io.odsp.domain.meta.MetaObjectCode;

import java.util.Optional;

public interface ChangeResult {
    default boolean isOk() {
        return false;
    }

    default Optional<Change> yield() {
        return Optional.empty();
    }

    static ChangeResult ok(Change change) {
        return new Ok(change);
    }

    static ChangeResult notEditable() {
        return new RevisionNotEditable();
    }

    static ChangeResult noSuchObject(MetaObjectCode code) {
        return new NoSuchObject(code);
    }

    record Ok(Change change) implements ChangeResult {
        @Override
        public boolean isOk() {
            return true;
        }

        @Override
        public Optional<Change> yield() {
            return Optional.of(change);
        }
    }

    record RevisionNotEditable() implements ChangeResult {
    }

    record NoSuchObject(MetaObjectCode code) implements ChangeResult {
    }

    record Cancelled() implements ChangeResult {
        @Override
        public boolean isOk() {
            return true;
        }
    }
}
