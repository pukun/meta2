package io.odsp.domain.revision;

import io.odsp.domain.meta.MetaObject;
import io.odsp.domain.model.Model;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RevisionEntryRepository extends Repository<MetaObject, Long> {
    @Query(
            value = """
                    select o.id id, code, edition, universal_flag, label, is_removed, created_time, last_updated_time, change_sequence,
                    from_entity_code, from_role, from_multiplicity, to_entity_code, to_role, to_multiplicity,
                    case
                        when a.id is not null then 1
                        when e.id is not null then 2
                        when o.id is not null then 0
                    end as  clazz_
                    from (
                        select o.*
                        from t_meta_object o, t_revision_change change
                        where product_object_id = o.id and revision_id = :#{#revision.id()}
                    union
                        select o.*
                        from t_meta_object o, t_model_entry me, t_model m
                        where o.id = me.object_id and me.model_id = m.id
                        and o.is_removed = false
                        and m.scope = :#{#revision.scope().name()} and m.edition = :#{#revision.edition().edition()}
                        and not exists(select 1 from t_revision_change where o.id = prototype_object_id and revision_id = :#{#revision.id()})
                    ) o
                    left outer join t_meta_association a on o.id = a.id
                    left outer join t_meta_entity e on o.id = e.id
                    """,
            nativeQuery = true
    )
    List<MetaObject> findAllObjectsInRevision(@Param("revision") Revision revision);

    @Query(
            value = """
                    select o.id, code, edition, universal_flag, label, is_removed, created_time, last_updated_time, change_sequence,
                    from_entity_code, from_role, from_multiplicity, to_entity_code, to_role, to_multiplicity,
                    case
                        when a.id is not null then 1
                        when e.id is not null then 2
                        when o.id is not null then 0
                    end as  clazz_
                    from
                    t_meta_object o left outer join t_meta_entity e on o.id = e.id left outer join t_meta_association a on o.id = a.id
                    where
                    o.id in (
                        select max(object.id) as id from
                        t_revision_change change join t_meta_object object on change.product_object_id = object.id
                        where
                        revision_id in (
                            select id from t_revision where
                            scope = :#{#local.scope().name()} and edition < :#{#local.edition().edition()}
                            and status = 'APPLIED'
                        )
                        and object.is_removed = true
                        and object.code in (
                            select code from t_model_entry join t_meta_object lo on object_id = lo.id where model_id = :#{#global.id()}
                        )
                        group by code
                    )
                    """
            ,
            nativeQuery = true
    )
    List<MetaObject> findRemovedReferencingObjects(@Param("local") Model local, @Param("global") Model global);

    @Query(
            value = """
                    select o.id, code, edition, universal_flag, label, is_removed, created_time, last_updated_time, change_sequence,
                    from_entity_code, from_role, from_multiplicity, to_entity_code, to_role, to_multiplicity,
                    case
                        when a.id is not null then 1
                        when e.id is not null then 2
                        when o.id is not null then 0
                    end as  clazz_
                    from
                    t_meta_object o left outer join t_meta_entity e on o.id = e.id left outer join t_meta_association a on o.id = a.id
                    where
                    o.id in (
                        select max(object.id) as id from
                        t_revision_change change join t_meta_object object on change.product_object_id = object.id
                        where
                        revision_id in (
                            select id from t_revision where
                            scope = :#{#global.scope().name()} and edition < :#{#global.edition().edition()}
                            and status = 'APPLIED'
                        )
                        and object.is_removed = true
                        and object.code in (
                            select code from t_model_entry join t_meta_object lo on object_id = lo.id where
                            model_id = :#{#local.id()} and lo.universal_flag = true
                        )
                        group by code
                    )
                    """
            ,
            nativeQuery = true
    )
    List<MetaObject> findRemovedReferencedObjects(@Param("local") Model local, @Param("global") Model global);
}
