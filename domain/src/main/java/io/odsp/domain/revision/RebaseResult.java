package io.odsp.domain.revision;

public interface RebaseResult {

    default boolean isOk() {
        return false;
    }

    static RebaseResult notEditable() {
        return new NotEditable();
    }

    static RebaseResult ok(int rebased) {
        return new Ok(rebased);
    }

    static RebaseResult alreadyBasedOnLatestModel() {
        return new AlreadyBasedOnLatestModel();
    }

    record Ok(int rebased) implements RebaseResult {

        public boolean isOk() {
            return true;
        }
    }

    record NotEditable() implements RebaseResult {
    }

    record AlreadyBasedOnLatestModel() implements RebaseResult {
    }
}
