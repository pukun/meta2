package io.odsp.domain.revision;

import io.odsp.domain.meta.*;
import io.odsp.domain.model.ModelEdition;
import io.odsp.domain.model.ModelIdentity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.util.Collections;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@DisplayName("individual change of objects")
public class RevisionChangeTest {
    private final RevisionContext context = Mockito.mock(RevisionContext.class);

    private final ArgumentCaptor<Change> changeCaptor = ArgumentCaptor.forClass(Change.class);

    private final long id = 100L;

    private final Revision revision = new Revision(ModelIdentity.of(ModelEdition.of(1)));

    private final MetaEntity entity = new MetaEntity();

    @BeforeEach
    public void setUp() {
        entity.id(100L)
                .code(MetaObjectCode.of("e1"))
                .label("l1")
                .edition(MetaObjectEdition.of(100));

        when(context.createNewObject(any(MetaObjectCreationSpec.class))).thenReturn(entity);
    }

    @Nested
    @DisplayName("creation of object")
    class CreationTest {
        @Test
        @DisplayName("refuse to change when revision is not editable")
        public void given_revision_is_archived_then_creation_is_not_allowed() {
            revision.status(RevisionStatus.ARCHIVED);

            ChangeResult result = revision.create(
                    new MetaEntityCreationSpec(true, "test", Collections.emptyList()),
                    context);

            assertThat(result instanceof ChangeResult.RevisionNotEditable, is(true));
        }

        @Test
        @DisplayName("record change for creation")
        public void given_revision_is_eligible_for_creation_then_a_new_change_is_generated() {

            ChangeResult result = revision.create(
                    new MetaEntityCreationSpec(true, "test", Collections.emptyList()),
                    context);

            assertThat(revision.commit(), is(1));

            assertThat(result.isOk(), is(true));
            assertThat(result.yield().map(Change::product), is(Optional.of(entity)));
        }
    }

    @Nested
    @DisplayName("revise existing objects")
    class ReviseTest {

        @Test
        @DisplayName("refuse to change when object doesn't exist")
        public void given_no_object_found_with_code_then_refuse_to_change() {
            when(context.prototypeOf(entity.code(), revision)).thenReturn(Optional.empty());

            ChangeResult result = revision.revise(entity.code(), entity.modificationSpecOf(), context);

            assertThat(result, instanceOf(ChangeResult.NoSuchObject.class));
        }

        @DisplayName("create change record when prototype hasn't been changed ")
        @Test
        public void given_prototype_has_not_been_changed_then_create_a_new_change() {
            when(context.changeOf(entity.code(), revision)).thenReturn(Optional.empty());
            when(context.prototypeOf(entity.code(), revision)).thenReturn(Optional.of(entity));

            ChangeResult result = revision.revise(entity.code(), entity.modificationSpecOf(), context);

            // a new change is created
            assertThat(result.yield().map(Change::product).map(MetaObject::edition), is(Optional.of(entity.edition().next())));
        }

        @Test
        @DisplayName("revise existing change record if it exists")
        public void given_prototype_has_been_changed_before_then_change_will_be_updated() {
            Change change = Mockito.mock(Change.class);
            when(context.changeOf(entity.code(), revision)).thenReturn(Optional.of(change));
            when(change.revise(entity.modificationSpecOf())).thenReturn(change);

            ChangeResult result = revision.revise(entity.code(), entity.modificationSpecOf(), context);

            assertThat(result.isOk(), is(true));

            ArgumentCaptor<MetaObjectModificationSpec> modificationSpecCaptor = ArgumentCaptor.forClass(MetaObjectModificationSpec.class);
            verify(change).revise(modificationSpecCaptor.capture());
            assertThat(modificationSpecCaptor.getValue(), is(entity.modificationSpecOf()));
        }
    }

    @Nested
    @DisplayName("remove existing objects")
    class RemoveTest {

        @Test
        @DisplayName("refuse to change when object doesn't exist")
        public void given_no_object_found_with_code_then_refuse_to_change() {
            when(context.prototypeOf(entity.code(), revision)).thenReturn(Optional.empty());

            ChangeResult result = revision.remove(entity.code(), context);

            assertThat(result, instanceOf(ChangeResult.NoSuchObject.class));
        }

        @DisplayName("create change record when prototype hasn't been changed ")
        @Test
        public void given_prototype_has_not_been_changed_then_create_a_new_change() {
            when(context.changeOf(entity.code(), revision)).thenReturn(Optional.empty());
            when(context.prototypeOf(entity.code(), revision)).thenReturn(Optional.of(entity));

            ChangeResult result = revision.remove(entity.code(), context);

            // a new change is created
            assertThat(result.yield().map(Change::product).map(MetaObject::edition), is(Optional.of(entity.edition().next())));
            assertThat(result.yield().map(Change::product).map(MetaObject::isRemoved), is(Optional.of(true)));
        }

        @Test
        @DisplayName("mark product as removed for existing change record ")
        public void given_prototype_has_been_changed_before_then_change_will_be_updated() {
            Change change = Mockito.mock(Change.class);
            when(context.changeOf(entity.code(), revision)).thenReturn(Optional.of(change));
            when(change.remove()).thenReturn(change);

            ChangeResult result = revision.remove(entity.code(), context);

            assertThat(result.isOk(), is(true));
            verify(change).remove();
        }
    }

    @Nested
    @DisplayName("resolve conflicts")
    class ResolveTest {

        @Test
        @DisplayName("refuse to change when prototype hasn't been changed ")
        public void given_prototype_has_not_been_changed_then_refuse_to_resolve() {
            when(context.changeOf(entity.code(), revision)).thenReturn(Optional.empty());

            ChangeResult result = revision.resolve(entity.code(), context);

            assertThat(result, instanceOf(ChangeResult.NoSuchObject.class));
        }

        @Test
        @DisplayName("mark conflict as resolved for existing change record ")
        public void given_prototype_has_been_changed_before_then_change_will_be_updated() {
            Change change = Mockito.mock(Change.class);
            when(context.changeOf(entity.code(), revision)).thenReturn(Optional.of(change));
            when(change.resolve()).thenReturn(change);

            ChangeResult result = revision.resolve(entity.code(), context);

            assertThat(result.isOk(), is(true));
            verify(change).resolve();
        }
    }

    @Nested
    @DisplayName("changes from reference")
    class ReferenceTest {

        @Test
        @DisplayName("create change record for referencing")
        public void generate_change_from_reference() {
            MetaObject reference = new MetaEntity()
                    .id(10L)
                    .label("uni")
                    .edition(MetaObjectEdition.of(3));

            Change change = revision.changeOfReference(reference);
            assertThat(change.prototype() == null, is(true));
            assertThat(change.reference(), is(reference));
            assertThat(change.product().code(), is(reference.code()));
            assertThat(change.product().label(), is(reference.label()));
            assertThat(change.product().edition(), is(MetaObjectEdition.InitialEdition));

            change = revision.changeOfReference(entity, reference);
            assertThat(change.prototype(), is(entity));
            assertThat(change.reference(), is(reference));
            assertThat(change.product().code(), is(reference.code()));
            assertThat(change.product().edition(), is(entity.edition().next()));
        }
    }
}
