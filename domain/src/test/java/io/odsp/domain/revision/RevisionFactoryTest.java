package io.odsp.domain.revision;

import io.odsp.domain.common.Scope;
import io.odsp.domain.meta.MetaObject;
import io.odsp.domain.meta.MetaObjectCode;
import io.odsp.domain.meta.MetaObjectEdition;
import io.odsp.domain.model.*;
import org.junit.jupiter.api.*;

import java.util.*;

import static java.util.function.Predicate.not;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.instanceOf;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class RevisionFactoryTest {
    private static final RevisionRepository repository = mock(RevisionRepository.class);
    private static final RevisionEntryRepository entryRepository = mock(RevisionEntryRepository.class);
    private static final ModelContext modelContext = mock(ModelContext.class);
    private static final RevisionContext context = mock(RevisionContext.class);

    private final RevisionFactory revisionFactory = new RevisionFactory(repository, context);

    @BeforeAll
    static void setUp() {
        when(context.modelContext()).thenReturn(modelContext);
        when(context.entryRepository()).thenReturn(entryRepository);
    }

    @Nested
    @DisplayName("create revision for universal model")
    class UniversalModel {

        @Test
        @DisplayName("when no model existed yet")
        public void given_no_model_existed_then_epoch_is_the_prototype() {
            when(modelContext.latestUniversalModel()).thenReturn(Optional.empty());
            assertThat(revisionFactory.revise().revision().isForEpochModel(), is(true));
        }

        @Test
        @DisplayName("when there is a universal model")
        public void given_a_latest_model_then_it_is_the_prototype() {
            Model universalModel = mock(Model.class);
            when(universalModel.edition()).thenReturn(ModelEdition.of(3));
            when(universalModel.identityOf()).thenReturn(ModelIdentity.of(ModelEdition.of(3)));
            when(modelContext.latestUniversalModel()).thenReturn(Optional.of(universalModel));

            Revision revision = revisionFactory.revise().revision();
            assertThat(revision.identityOfPrototypeModel(), equalTo(universalModel.identityOf()));
        }
    }

    @Nested
    @DisplayName("create revision for proprietary model")
    class ProprietaryModel {

        private final Model universalModel = mock(Model.class);

        @BeforeEach
        public void setUp() {
            when(universalModel.edition()).thenReturn(new ModelEdition(3));
            when(modelContext.latestUniversalModel()).thenReturn(Optional.of(universalModel));

            MetaObject a = mock(MetaObject.class);
            when(a.code()).thenReturn(MetaObjectCode.of("a"));
            when(a.successorOf()).thenReturn(a);
            when(a.edition(any(MetaObjectEdition.class))).thenReturn(a);

            MetaObject b = mock(MetaObject.class);
            when(b.code()).thenReturn(MetaObjectCode.of("b"));
            when(b.successorOf()).thenReturn(b);
            when(b.edition(any(MetaObjectEdition.class))).thenReturn(b);

            when(modelContext.findActiveObjectsInModel(universalModel)).thenReturn(java.util.Arrays.asList(
                    a, b
            ));
        }

        @Test
        @DisplayName("when no model existed yet")
        public void given_no_proprietary_model_in_scope_then_epoch_is_the_prototype() {
            when(modelContext.latestModelInScope(Scope.JX)).thenReturn(Optional.empty());

            RevisionCreation.Ok result = revisionFactory.revise(Scope.JX);

            assertThat(result.revision().identityOfPrototypeModel(), equalTo(ModelIdentity.epoch(Scope.JX)));
            assertThat(result.revision().getReferenceModel().map(Model::edition), equalTo(Optional.of(universalModel.edition())));

            assertThat(result.changes().stream().map(Change::product).map(MetaObject::code).map(MetaObjectCode::code).toList(), containsInAnyOrder("a", "b"));
        }
    }

    @Nested
    @DisplayName("upgrade proprietary model")
    class Upgrade {

        @Nested
        @DisplayName("upgrade in some edge cases")
        class EdgeCases {

            @Test
            @DisplayName("should refuse to upgrade universal model")
            public void should_refuse_upgrade_universal_model() {
                RevisionCreation result = revisionFactory.upgrade(Scope.UNIVERSALITY, ModelEdition.epoch());

                assertThat(result instanceof RevisionCreation.NotProprietaryScope, is(true));
            }

            @Test
            @DisplayName("should refuse to upgrade when there is not proprietary model")
            public void given_proprietary_mode_does_not_exist_then_refuse_upgrade() {
                // given no proprietary model existed yet
                when(modelContext.latestModelInScope(Scope.JX)).thenReturn(Optional.empty());

                // and no universal model
                when(modelContext.latestUniversalModel()).thenReturn(Optional.empty());

                // then upgrade is not possible
                RevisionCreation result = revisionFactory.upgrade(Scope.JX, ModelEdition.epoch());
                assertThat(result, instanceOf(RevisionCreation.NoModelInScope.class));
            }

            @Test
            @DisplayName("should refuse to upgrade when there is not universal model")
            public void given_universal_mode_does_not_exist_then_refuse_upgrade() {
                // given proprietary model is available
                when(modelContext.latestModelInScope(Scope.JX)).thenReturn(Optional.of(mock(Model.class)));

                // but no universal model at the specified edition
                when(modelContext.findModelByIdentity(ModelIdentity.epoch())).thenReturn(Optional.empty());

                // then upgrade is not possible
                RevisionCreation result = revisionFactory.upgrade(Scope.JX, ModelEdition.epoch());
                assertThat(result, instanceOf(RevisionCreation.NoSuchReferenceModel.class));
            }

            @Test
            @DisplayName("should refuse when proprietary model already uses the latest model")
            public void given_proprietary_model_is_already_up_to_date_then_refuse_upgrade() {
                Model universal = mock(Model.class);
                Model proprietary = mock(Model.class);
                ModelEdition upgradeToEdition = ModelEdition.of(11);

                // given both proprietary model and universal model are available
                when(modelContext.latestModelInScope(Scope.JX)).thenReturn(Optional.of(proprietary));
                when(modelContext.findModelByIdentity(ModelIdentity.of(upgradeToEdition))).thenReturn(Optional.of(universal));

                // but proprietary model is already up-to-date
                when(proprietary.canBeUpgradedTo(universal)).thenReturn(false);

                RevisionCreation result = revisionFactory.upgrade(Scope.JX, upgradeToEdition);

                assertThat(result, instanceOf(RevisionCreation.AlreadyUpToDate.class));
            }
        }


        @Nested
        @DisplayName("verify we can generate changes as expected in upgrade process")
        class ChangeGenerationCases {
            private final Model universalModel = mock(Model.class);
            private final Model proprietaryModel = mock(Model.class);

            @BeforeEach
            public void setUp() {
                when(proprietaryModel.identityOf()).thenReturn(ModelIdentity.of(Scope.SH, ModelEdition.of(3)));
            }


            @Test
            @DisplayName("check initialization of model delta")
            public void verify_model_delta_could_be_generated() {
                MetaObject u1 = objectOf("u1");
                when(modelContext.findActiveObjectsInModel(universalModel))
                        .thenReturn(Collections.singletonList(u1));

                MetaObject p1 = objectOf("p1");
                when(modelContext.findActiveUniversalObjectsInModel(proprietaryModel))
                        .thenReturn(Collections.singletonList(p1));

                MetaObject u2 = objectOf("u2");
                when(entryRepository.findRemovedReferencedObjects(proprietaryModel, universalModel))
                        .thenReturn(Collections.singletonList(u2));

                MetaObject p2 = objectOf("p2");
                when(entryRepository.findRemovedReferencingObjects(proprietaryModel, universalModel))
                        .thenReturn(Collections.singletonList(p2));

                RevisionFactory.ModelDelta modelDelta = revisionFactory.generateDeltaBetweenModels(proprietaryModel, universalModel);

                assertThat(codesOf(modelDelta.activeReferencedObjects()), containsInAnyOrder(u1.code()));
                assertThat(codesOf(modelDelta.activeReferencingObjects()), containsInAnyOrder(p1.code()));
                assertThat(codesOf(modelDelta.removedReferencedObjects()), containsInAnyOrder(u2.code()));
                assertThat(codesOf(modelDelta.removedReferencingObjects()), containsInAnyOrder(p2.code()));
            }

            @Test
            @DisplayName("check generation of changes")
            public void check_changes_could_be_generated() {
                Revision revision = revisionFactory.updateOf(proprietaryModel, universalModel);

                RevisionFactory.ModelDelta modelDelta = new RevisionFactory.ModelDelta(
                        objectsOf("u1", "u2", "u3"),
                        objectsOf("u2", "u4"),
                        objectsOf("u0", "u4"),
                        objectsOf("u0", "u1")
                );

                List<Change> changes = revisionFactory.generateChangesForUpgrade(revision, modelDelta);

                assertThat(changes.size(), is(4));
                assertThat(referenceCodesOf(changes), containsInAnyOrder("u1", "u2", "u3", "u4"));
                assertThat(prototypeCodesOf(changes), containsInAnyOrder("u1", "u2", "u4"));
            }
        }

        private List<MetaObject> objectsOf(String... codes) {
            List<MetaObject> objects = new ArrayList<>();
            for (String code : codes) {
                objects.add(objectOf(code));
            }

            return objects;
        }

        private MetaObject objectOf(String code) {
            MetaObject o = mock(MetaObject.class);
            when(o.code()).thenReturn(MetaObjectCode.of(code));
            when(o.successorOf()).thenReturn(o);
            when(o.edition(any(MetaObjectEdition.class))).thenReturn(o);
            when(o.edition()).thenReturn(MetaObjectEdition.of(1));

            return o;
        }

        private List<MetaObjectCode> codesOf(List<MetaObject> objects) {
            return objects.stream().map(MetaObject::code)
                    .toList();
        }

        private List<String> referenceCodesOf(List<Change> changes) {
            return changes.stream().map(Change::reference)
                    .filter(not(Objects::isNull))
                    .map(MetaObject::code)
                    .map(MetaObjectCode::code)
                    .toList();
        }

        private List<String> prototypeCodesOf(List<Change> changes) {
            return changes.stream().map(Change::prototype)
                    .filter(not(Objects::isNull))
                    .map(MetaObject::code)
                    .map(MetaObjectCode::code)
                    .toList();
        }
    }
}
