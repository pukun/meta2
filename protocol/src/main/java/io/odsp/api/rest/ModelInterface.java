package io.odsp.api.rest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RequestMapping("/v1/models")
public interface ModelInterface {

    @PostMapping("/{modelId}/release")
    Mono<Void> release(@PathVariable("modelId") Long modelId);

    @GetMapping("/{scope}/latest")
    Flux<MetaObjectSpec> objects(@PathVariable("scope") String scope);

    @PostMapping("/{modelId}/revise")
    Mono<ResponseEntity<Revision>> revise(@RequestBody ModelId modelId);
}
