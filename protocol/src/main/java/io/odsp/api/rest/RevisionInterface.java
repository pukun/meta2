package io.odsp.api.rest;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RequestMapping(value = "/v1/revisions", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public interface RevisionInterface {

    @GetMapping("/{revisionId}/objects")
    Flux<MetaObjectSpec> getObjects(@PathVariable("revisionId") Long revisionId);

    @PostMapping("/{revisionId}/objects")
    Mono<MetaObjectSpec> createObject(@PathVariable("revisionId") Long revisionId, @RequestBody MetaObjectSpec spec);


    @PostMapping("/{revisionId}/apply")
    Mono<ResponseEntity<Revision>> apply(@PathVariable("revisionId") Long revisionId);


}
