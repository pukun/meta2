package io.odsp.api.rest;

import lombok.Builder;

@Builder
public record ModelId(String scope, int edition) {
}
