package io.odsp.api.rest;

public record ApplicationError(String code, String message) {
}
