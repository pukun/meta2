package io.odsp.api.rest;

import java.util.Date;
import java.util.List;

public record MetaEntitySpec(
        String code, String origin, String label, int edition, Date createdTime, boolean isRemoved,
        List<MetaEntityAttributeRecord> attributes
) implements MetaObjectSpec {
}