package io.odsp.api.rest;

import lombok.Builder;

import java.util.Date;

@Builder(toBuilder = true)
public record Draft(
        MetaObjectSpec prototype,
        MetaObjectSpec product,
        boolean isConflicting,
        Date lastUpdatedTime
) {
}