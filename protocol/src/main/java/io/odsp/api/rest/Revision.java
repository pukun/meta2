package io.odsp.api.rest;

import lombok.Builder;

import java.util.Date;

@Builder
public record Revision(
        Long id,
        ModelId revisedModel,
        ModelId referenceModel,
        ModelId producedModel,
        String status,
        Date lastUpdatedTime
) {

}
