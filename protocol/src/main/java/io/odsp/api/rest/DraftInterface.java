package io.odsp.api.rest;

import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RequestMapping(value = "/v1/revisions/{revisionId}/objects/{objectCode}/draft")
public interface DraftInterface {

    @GetMapping
    Mono<Draft> draftOf(@PathVariable("revisionId") Long revisionId, @PathVariable("objectCode") String objectCode);

    @PatchMapping(path = "/revise")
    Mono<Draft> revise(@PathVariable("revisionId") Long revisionId, @PathVariable("objectCode") Long objectCode, @RequestBody MetaObjectSpec spec);

    @PatchMapping(path = "/reset")
    Mono<Draft> reset(@PathVariable("revisionId") Long revisionId, @PathVariable("objectCode") Long objectCode);

    @PatchMapping(path = "/remove")
    Mono<Draft> remove(@PathVariable("revisionId") Long revisionId, @PathVariable("objectCode") Long objectCode);

    @PatchMapping(path = "/resolve")
    Mono<Draft> resolve(@PathVariable("revisionId") Long revisionId, @PathVariable("objectCode") Long objectCode);
}
