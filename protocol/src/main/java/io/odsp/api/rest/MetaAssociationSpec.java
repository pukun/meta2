package io.odsp.api.rest;

import java.util.Date;

public record MetaAssociationSpec(
        String code, String origin, String label, int edition, Date createdTime,
        String isRemoved, String fromEntity, String fromRole, String fromMultiplicity,
        String toEntity, String toRole, String toMultiplicity
) implements MetaObjectSpec {
}
