package io.odsp.api.rest;

public record MetaEntityAttributeRecord(String name, String type) {
}
