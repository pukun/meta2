package io.odsp.service.model;

import io.odsp.domain.model.ModelContext;
import io.odsp.domain.model.ModelEntryRepository;
import io.odsp.domain.model.ModelFactory;
import io.odsp.domain.model.ModelRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ModelConfig {

    @Bean
    public ModelContext modelContext(ModelRepository modelRepository, ModelEntryRepository modelEntryRepository) {
        return new ModelContext(new ModelFactory(), modelRepository, modelEntryRepository);
    }
}
