package io.odsp.service.meta;

import io.odsp.domain.meta.MetaObjectFactory;
import io.odsp.domain.meta.MetaObjectRepository;
import io.odsp.domain.meta.MetaObjectCodeGenerator;
import io.odsp.domain.meta.MetaObjectContext;
import io.odsp.infra.SequenceMetaObjectCodeGenerator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.persistence.EntityManager;

@Configuration
public class MetaObjectConfig {

    @Bean
    public MetaObjectCodeGenerator metaObjectCodeGenerator(EntityManager em, @Value("${app.meta.object.code.sequence:seq_meta}") String sequnece) {
        return new SequenceMetaObjectCodeGenerator(em, sequnece);
    }

    @Bean
    public MetaObjectContext metaObjectContext(MetaObjectCodeGenerator metaObjectCodeGenerator, MetaObjectRepository metaObjectRepository) {
        return new MetaObjectContext(new MetaObjectFactory(metaObjectCodeGenerator), metaObjectRepository);
    }
}
