package io.odsp.service.revision;

import io.odsp.domain.meta.MetaObjectContext;
import io.odsp.domain.model.ModelContext;
import io.odsp.domain.revision.ChangeRepository;
import io.odsp.domain.revision.RevisionContext;
import io.odsp.domain.revision.RevisionEntryRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RevisionConfig {

    @Bean
    public RevisionContext context(
            MetaObjectContext metaObjectContext,
            ModelContext modelContext,
            RevisionEntryRepository entryRepository,
            ChangeRepository changeRepository
    ) {
        return new RevisionContext(metaObjectContext, modelContext, entryRepository, changeRepository);
    }
}
