package io.odsp.service.revision;

import io.odsp.api.model.ModelIdMapper;
import io.odsp.api.rest.Revision;
import io.odsp.domain.model.Model;

public class RevisionMapper {
    static Revision from(io.odsp.domain.revision.Revision revision) {
        return Revision.builder()
                .id(revision.id())
                .revisedModel(ModelIdMapper.from(revision.identityOfPrototypeModel()))
                .producedModel(ModelIdMapper.from(revision.identityOfPrototypeModel().next()))
                .referenceModel(revision.getReferenceModel()
                        .map(Model::identityOf)
                        .map(ModelIdMapper::from)
                        .orElse(null)
                ).status(revision.status().name())
                .lastUpdatedTime(revision.getLastUpdatedTime())
                .build();
    }
}
