package io.odsp.service.revision;

import io.odsp.domain.revision.ApplyingFailure;
import io.odsp.domain.revision.Revision;
import io.odsp.domain.revision.RevisionContext;
import io.odsp.domain.revision.RevisionRepository;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.Optional;

@Service
public class RevisionService {
    private static final Logger logger = LoggerFactory.getLogger(RevisionService.class);

    @Autowired
    private PlatformTransactionManager txManager;

    @Autowired
    private RevisionRepository revisionRepository;

    @Autowired
    private RevisionContext revisionContext;

    public Pair<io.odsp.api.rest.Revision, Optional<ApplyingFailure>> apply(long revisionId) {
        logger.info("Trying to merge revision [{}]...", revisionId);

        return revisionRepository.findById(revisionId)
                .map(this::apply)
                .map(r -> Pair.of(RevisionMapper.from(r.getLeft()), r.getRight()))
                .orElseGet(() -> {
                    logger.info("Failed to adopt revision(id={}) since there is no such revision", revisionId);
                    return Pair.of(null, Optional.of(ApplyingFailure.notFound(revisionId)));
                });
    }

    private Pair<Revision, Optional<ApplyingFailure>> apply(Revision revision) {
        return new TransactionTemplate(txManager).execute(status -> {
            Optional<ApplyingFailure> applyingFailure = revision.apply(revisionContext);
            logger.info("Revision(id=[{}]) {})", revision.id(), applyingFailure
                    .map(failure -> String.format("couldn't be applied(%s)", failure.detail()))
                    .orElse("is applied successfully"));

            if (applyingFailure.isPresent()) status.setRollbackOnly();
            else revisionRepository.save(revision);

            return Pair.of(revision, applyingFailure);
        });
    }
}
