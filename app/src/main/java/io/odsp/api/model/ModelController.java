package io.odsp.api.model;

import io.odsp.api.rest.MetaObjectSpec;
import io.odsp.api.rest.ModelId;
import io.odsp.api.rest.ModelInterface;
import io.odsp.api.rest.Revision;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class ModelController implements ModelInterface {
    @Override
    public Mono<Void> release(Long modelId) {
        return Mono.empty();
    }

    @Override
    public Flux<MetaObjectSpec> objects(String modelScope) {
        return Flux.empty();
    }

    @Override
    public Mono<ResponseEntity<Revision>> revise(ModelId modelId) {
        return null;
    }
}
