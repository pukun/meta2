package io.odsp.api.model;

import io.odsp.api.rest.ModelId;
import io.odsp.domain.model.ModelIdentity;

public class ModelIdMapper {
    public static ModelId from(ModelIdentity modelIdentity) {
        return ModelId.builder()
                .scope(modelIdentity.scope().name())
                .edition(modelIdentity.edition().edition())
                .build();
    }
}
