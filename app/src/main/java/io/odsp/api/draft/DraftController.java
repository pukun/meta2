package io.odsp.api.draft;

import io.odsp.api.rest.Draft;
import io.odsp.api.rest.DraftInterface;
import io.odsp.api.rest.MetaObjectSpec;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
public class DraftController implements DraftInterface {

    public Mono<Draft> draftOf(Long revisionId, String objectCode) {
        return null;
    }

    @Override
    public Mono<Draft> revise(Long revisionId, Long draftId, MetaObjectSpec spec) {
        return Mono.empty();
    }

    @Override
    public Mono<Draft> reset(Long revisionId, Long draftId) {
        return Mono.empty();
    }

    @Override
    public Mono<Draft> remove(Long revisionId, Long objectCode) {
        return null;
    }

    @Override
    public Mono<Draft> resolve(Long revisionId, Long objectCode) {
        return null;
    }
}
