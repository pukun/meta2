package io.odsp.api.revision;

import io.odsp.api.rest.MetaObjectSpec;
import io.odsp.api.rest.Revision;
import io.odsp.api.rest.RevisionInterface;
import io.odsp.domain.revision.ApplyingFailure;
import io.odsp.service.revision.RevisionService;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;

import java.util.Optional;
import java.util.function.Function;

@RestController
public class RevisionController implements RevisionInterface {
    private static final Logger logger = LoggerFactory.getLogger(RevisionController.class);

    private final RevisionService revisionService;

    private final Scheduler scheduler;


    public RevisionController(RevisionService revisionService, Scheduler scheduler) {
        this.revisionService = revisionService;
        this.scheduler = scheduler;
    }


    @Override
    public Flux<MetaObjectSpec> getObjects(Long revisionId) {
        return null;
    }

    @Override
    public Mono<MetaObjectSpec> createObject(Long revisionId, MetaObjectSpec spec) {
        return null;
    }

    @Override
    public Mono<ResponseEntity<Revision>> apply(Long revisionId) {
        Function<Pair<Revision, Optional<ApplyingFailure>>, Revision> resultProcessor = r -> {
            if (r.getRight().isPresent()) throw new ApplyingException(r.getRight().get());
            else return r.getLeft();
        };

        return Mono.fromCallable(() -> revisionService.apply(revisionId))
                .subscribeOn(scheduler)
                .map(resultProcessor)
                .map(ResponseEntity::ok);
    }

    @ExceptionHandler
    public ResponseEntity<io.odsp.api.rest.ApplicationError> onApplyingException(ApplyingException me) {
        return ResponseEntity.status(me.status())
                .body(me.error());
    }
}
