package io.odsp.api.revision;

import io.odsp.domain.revision.ApplyingFailure;
import org.springframework.http.HttpStatus;

class ApplyingException extends RuntimeException {
    private final ApplyingFailure failure;

    public ApplyingException(ApplyingFailure failure) {
        this.failure = failure;
    }

    @Override
    public String getMessage() {
        return error().message();
    }

    public io.odsp.api.rest.ApplicationError error() {
        if (failure instanceof ApplyingFailure.UnexpectedFailure ue) return new io.odsp.api.rest.ApplicationError("UNKNOWN_ERROR", ue.detail());
        else if (failure instanceof ApplyingFailure.NotFound notFound)
            return new io.odsp.api.rest.ApplicationError("notFound", notFound.detail());
        else if (failure instanceof ApplyingFailure.NotAllowed notAllowed)
            return new io.odsp.api.rest.ApplicationError("notAllowed", notAllowed.detail());
        else if (failure instanceof ApplyingFailure.WithConflict withConflict)
            return new io.odsp.api.rest.ApplicationError("withConflict", withConflict.detail());
        else if (failure instanceof ApplyingFailure.NotTargetTheLatestModel notTargetTheLatestModel)
            return new io.odsp.api.rest.ApplicationError("staleModel", notTargetTheLatestModel.detail());
        else return new io.odsp.api.rest.ApplicationError("UNKNOWN_ERROR", "");
    }

    public HttpStatus status() {
        if (failure instanceof ApplyingFailure.UnexpectedFailure ue) return HttpStatus.INTERNAL_SERVER_ERROR;
        else if (failure instanceof ApplyingFailure.NotFound notFound) return HttpStatus.NOT_FOUND;
        else if (failure instanceof ApplyingFailure.NotAllowed notAllowed) return HttpStatus.UNPROCESSABLE_ENTITY;
        else if (failure instanceof ApplyingFailure.WithConflict withConflict) return HttpStatus.UNPROCESSABLE_ENTITY;
        else if (failure instanceof ApplyingFailure.NotTargetTheLatestModel notTargetTheLatestModel) return HttpStatus.UNPROCESSABLE_ENTITY;
        else return HttpStatus.INTERNAL_SERVER_ERROR;
    }
}
