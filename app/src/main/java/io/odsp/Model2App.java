package io.odsp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Model2App {
    private static final Logger logger = LoggerFactory.getLogger(Model2App.class);

    public static void main(String[] args) {
        SpringApplication.run(Model2App.class, args);

        logger.info("Model2 application is started");
    }
}
