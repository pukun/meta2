package io.odsp.infra;

import io.odsp.domain.meta.MetaObjectCode;
import io.odsp.domain.meta.MetaObjectCodeGenerator;
import io.odsp.domain.meta.MetaObjectType;

import javax.persistence.EntityManager;
import java.math.BigInteger;

public class SequenceMetaObjectCodeGenerator implements MetaObjectCodeGenerator {

    private final EntityManager em;
    private final String query;

    public static final String DEFAULT_META_FORMAT = "%s-%09d";

    public SequenceMetaObjectCodeGenerator(EntityManager em, String sequence) {
        this.em = em;
        this.query = String.format("select nextval('%s') ", sequence);
    }

    @Override
    public MetaObjectCode code(MetaObjectType objectType) {
        BigInteger next = (BigInteger) em.createNativeQuery(query).getSingleResult();
        return MetaObjectCode.of(String.format(
                DEFAULT_META_FORMAT,
                objectType.tag(), next.longValue()
        ));
    }
}
