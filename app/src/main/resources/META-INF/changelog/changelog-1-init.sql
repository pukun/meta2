--liquibase formatted sql

--changeset colin:1
create sequence seq_revision increment by 1 minvalue 1 maxvalue 9999999 start with 1;
create sequence seq_model increment by 1 minvalue 1 maxvalue 9999999 start with 1;
create sequence seq_meta increment by 50 minvalue 1 maxvalue 9999999 start with 1;
create sequence seq_change increment by 50 minvalue 1 maxvalue 9999999 start with 1;
create table t_revision (
  id                 int8 not null,
  scope              varchar(16) not null,
  edition            int4 not null,
  reference_model_id int8,
  status             varchar(16) not null,
  "commit"           int4 not null,
  product_model_id   int8,
  created_time       timestamp default current_timestamp not null,
  last_updated_time  timestamp default current_timestamp not null,
  change_sequence    int4 not null,
  primary key (id));
comment on table t_revision is '模型修订';
comment on column t_revision.scope is '被修订模型的管理域';
comment on column t_revision.edition is '被修订模型的序列号';
comment on column t_revision.reference_model_id is '如果当前修订模型是私有模型，本次修订使用的全局模型。在修改全局模型的时候，本字段为空。';
comment on column t_revision."commit" is '修订的提交序列号。每次修改实体或者关系时递增1。';
comment on column t_revision.product_model_id is '修订被采用后生成的模型';
create table t_model_entry (
  model_id     int8 not null,
  object_Id    int8 not null,
  created_time timestamp default current_timestamp not null,
  primary key (model_id,
  object_Id));
create table t_model (
  id                 int8 not null,
  scope              varchar(16) not null,
  edition            int4 not null,
  latest_flag        bool default 'false',
  version            varchar(32),
  reference_model_id int8,
  created_time       timestamp default current_timestamp not null,
  last_updated_time  timestamp default current_timestamp not null,
  change_sequence    int4 not null,
  primary key (id));
comment on column t_model.latest_flag is '本模型是否是管理域内的最新模型';
create table t_meta_association (
  id                int8 not null,
  from_entity_code  varchar(255) not null,
  from_role         varchar(255),
  from_multiplicity varchar(16),
  to_entity_code    varchar(255) not null,
  to_role           varchar(255),
  to_multiplicity   varchar(16),
  primary key (id));
comment on table t_meta_association is '实体关系';
comment on column t_meta_association.from_entity_code is 'from端实体代码';
comment on column t_meta_association.to_entity_code is 'to端实体代码';
create table t_meta_entity_attribute (
  entity_id int8 not null,
  sequence  int4 not null,
  label     varchar(255) not null,
  type      varchar(32) not null,
  primary key (entity_id,
  sequence));
create table t_meta_entity (
  id int8 not null,
  primary key (id));
comment on table t_meta_entity is '实体规格';
create table t_meta_object (
  id                int8 not null,
  code              varchar(255) not null,
  universal_flag    bool not null,
  edition           int4 not null,
  label             varchar(255) not null,
  is_removed        bool,
  created_time      timestamp default current_timestamp not null,
  last_updated_time timestamp default current_timestamp not null,
  change_sequence   int4 not null,
  primary key (id));
comment on table t_meta_object is '实体规格';
comment on column t_meta_object.universal_flag is '是否为全局对象';
comment on column t_meta_object.edition is '在域中的对象版本序号';
comment on column t_meta_object.label is '对象标签';
comment on column t_meta_object.is_removed is '是否被移除';
comment on column t_meta_object.change_sequence is '乐观锁';
create table t_revision_change (
  id                  bigserial not null,
  revision_id         int8 not null,
  prototype_object_id int8,
  reference_object_id int8,
  product_object_id   int8 not null,
  conflict_flag       bool not null,
  created_time        timestamp default current_timestamp,
  last_updated_time   timestamp default current_timestamp not null,
  change_sequence     int4 default 1 not null,
  primary key (id));
comment on table t_revision_change is '修订中包含的变更';
comment on column t_revision_change.prototype_object_id is '变更使用的原型对象(属于被修订模型)。为null时表示当前变更是新建对象。';
comment on column t_revision_change.reference_object_id is '变更引用的外部对象（来自于通用模型或者导入模型）。';
comment on column t_revision_change.product_object_id is '变更产生的结果对象';
comment on column t_revision_change.conflict_flag is '变更的结果对象是否和原型对象有冲突';
create index idx_revision_base_model
  on t_revision (scope, edition);
create index idx_generated_model
  on t_revision (product_model_id);
create unique index idx_model_scope_edition
  on t_model (scope, edition);
create unique index t_meta_association_id
  on t_meta_association (id);
create unique index idx_entity_attribute_sequence
  on t_meta_entity_attribute (entity_id, sequence);
create unique index t_meta_entity_id
  on t_meta_entity (id);
create index idx_object_code
  on t_meta_object (code);
create unique index t_meta_object_id
  on t_meta_object (id);
create index t_meta_object_code
  on t_meta_object (code);
create index idx_revision_change_revision
  on t_revision_change (revision_id);
create index idx_revision_change_prototype
  on t_revision_change (prototype_object_id);
create index idx_revision_change_product
  on t_revision_change (product_object_id);
create index idx_revision_change_reference
  on t_revision_change (reference_object_id);
